package pt.ipca.europeanlemansseries.VolleyHelper

import android.content.Context
import com.android.volley.Request
import com.android.volley.Request.Method.POST
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject


class VolleyHelper {

    companion object {
        private val SERVER_API = "http://192.168.1.8:5000"
        private val TIMEOUT = 5000

        private var mInstance = VolleyHelper()

        @Synchronized
        fun getInstance(): VolleyHelper {
            if (mInstance.equals(null)) {
                mInstance = VolleyHelper()
            }
            return mInstance
        }
    }

    private lateinit var queue : RequestQueue

    interface OnGetDataListener {
        fun onSucess(data: String) : Unit
        fun onError(error: String) : Unit
    }

    interface OnAddListener {
        fun onSucess(data: String) : Unit
        fun onError(error: String) : Unit
    }

    fun add(context: Context?, path: String, jsonObject: JSONObject?, listener: OnAddListener) {
        queue = Volley.newRequestQueue(context)
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.POST, SERVER_API + path,
            jsonObject,
            {
                    response -> listener.onSucess(response.toString())
            }
        )
        {
                error -> listener.onError(error.toString())
        }
        queue.add(jsonObjectRequest)
    }

    fun login(context: Context?, path: String, jsonObject: JSONObject?, listener: OnAddListener) {
        queue = Volley.newRequestQueue(context)
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.POST, SERVER_API + path,
            jsonObject,
            {
                response -> listener.onSucess(response.toString())
            }
        )
        {
            error -> listener.onSucess(error.toString())
        }
        queue.add(jsonObjectRequest)
    }

    fun getAllRaces(context: Context?, path: String, listener: OnGetDataListener) {
        queue = Volley.newRequestQueue(context)
        val stringRequest = StringRequest(
            Request.Method.POST, SERVER_API + path,
            { response ->
                listener.onSucess(response.toString())
            }
        )
        { error ->
            listener.onSucess(error.toString())
        }
        queue.add(stringRequest)
    }

    fun getLastTreeNotices(context: Context?, path: String, listener: OnGetDataListener) {
        queue = Volley.newRequestQueue(context)
        val stringRequest = StringRequest(
                Request.Method.POST, SERVER_API + path,
                { response ->
                    listener.onSucess(response.toString())
                }
        )
        { error ->
            listener.onSucess(error.toString())
        }
        queue.add(stringRequest)
    }

    fun getAllNotices(context: Context?, path: String, listener: OnGetDataListener) {
        queue = Volley.newRequestQueue(context)
        val stringRequest = StringRequest(
                Request.Method.POST, SERVER_API + path,
                { response ->
                    listener.onSucess(response.toString())
                }
        )
        { error ->
            listener.onSucess(error.toString())
        }
        queue.add(stringRequest)
    }

    fun getAllTeams(context: Context?, path: String, listener: OnGetDataListener) {
        queue = Volley.newRequestQueue(context)
        val stringRequest = StringRequest(
            Request.Method.POST, SERVER_API + path,
            { response ->
                listener.onSucess(response.toString())
            }
        )
        { error ->
            listener.onSucess(error.toString())
        }
        queue.add(stringRequest)
    }

    fun getTeamCars(context: Context?, path: String, id: Int, listener: OnGetDataListener) {
        queue = Volley.newRequestQueue(context)
        val stringRequest = StringRequest(
                Request.Method.POST, SERVER_API + path + id,
                { response ->
                    listener.onSucess(response.toString())
                }
        )
        { error ->
            listener.onSucess(error.toString())
        }
        queue.add(stringRequest)
    }

    fun getTeam(context: Context?, path: String, id: Int,listener: OnGetDataListener) {
        queue = Volley.newRequestQueue(context)
        val stringRequest = StringRequest(
                Request.Method.POST, SERVER_API + path + id,
                { response ->
                    listener.onSucess(response.toString())
                }
        )
        { error ->
            listener.onSucess(error.toString())
        }
        queue.add(stringRequest)
    }

    fun getCars(context: Context?, path: String, listener: OnGetDataListener) {
        queue = Volley.newRequestQueue(context)
        val stringRequest = StringRequest(
            Request.Method.POST, SERVER_API + path,
            { response ->
                listener.onSucess(response.toString())
            }
        )
        { error ->
            listener.onSucess(error.toString())
        }
        queue.add(stringRequest)
    }

    fun getCarDrivers(context: Context?, path: String, id: Int,listener: OnGetDataListener) {
        queue = Volley.newRequestQueue(context)
        val stringRequest = StringRequest(
            Request.Method.POST, SERVER_API + path + id,
            { response ->
                listener.onSucess(response.toString())
            }
        )
        { error ->
            listener.onSucess(error.toString())
        }
        queue.add(stringRequest)
    }

    fun getDrivers(context: Context?, path: String, listener: OnGetDataListener) {
        queue = Volley.newRequestQueue(context)
        val stringRequest = StringRequest(
            Request.Method.POST, SERVER_API + path,
            { response ->
                listener.onSucess(response.toString())
            }
        )
        { error ->
            listener.onSucess(error.toString())
        }
        queue.add(stringRequest)
    }

    fun getRaceSessons(context: Context?, path: String, id: Int,listener: OnGetDataListener) {
        queue = Volley.newRequestQueue(context)
        val stringRequest = StringRequest(
            Request.Method.POST, SERVER_API + path + id,
            { response ->
                listener.onSucess(response.toString())
            }
        )
        { error ->
            listener.onSucess(error.toString())
        }
        queue.add(stringRequest)
    }

    fun getRaceResult(context: Context?, path: String, id: Int,listener: OnGetDataListener) {
        queue = Volley.newRequestQueue(context)
        val stringRequest = StringRequest(
                Request.Method.POST, SERVER_API + path + id,
                { response ->
                    listener.onSucess(response.toString())
                }
        )
        { error ->
            listener.onSucess(error.toString())
        }
        queue.add(stringRequest)
    }

    fun getClassification(context: Context?, path: String, listener: OnGetDataListener) {
        queue = Volley.newRequestQueue(context)
        val stringRequest = StringRequest(
            Request.Method.POST, SERVER_API + path,
            { response ->
                listener.onSucess(response.toString())
            }
        )
        { error ->
            listener.onSucess(error.toString())
        }
        queue.add(stringRequest)
    }
}