package pt.ipca.europeanlemansseries.Activitys

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import pt.ipca.europeanlemansseries.Backend
import pt.ipca.europeanlemansseries.Models.Utilizador
import pt.ipca.europeanlemansseries.R

class LoginActivity : AppCompatActivity() {

    // Widgets
    lateinit var editTextEmail: EditText
    lateinit var editTextPassword: EditText
    lateinit var buttonLogin: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        editTextEmail = findViewById(R.id.editTextLoginEmail)
        editTextPassword = findViewById(R.id.editTextLoginPassword)
        buttonLogin = findViewById(R.id.buttonLogin)

        buttonLogin.setOnClickListener {

            /* Criar Dialog */
            val builder: AlertDialog.Builder = AlertDialog.Builder(this@LoginActivity)

            val inflater: LayoutInflater = this@LoginActivity.layoutInflater
            val view = inflater.inflate(R.layout.custom_dialog_loading, null)
            builder.setView(view)

            val imageViewGif = view.findViewById<ImageView>(R.id.imageView3)
            Glide.with(this).load(R.drawable.elms_gift).into(imageViewGif)

            val dialog = builder.create()
            dialog.show() /* Fim */


            // guardar os valores das editTexts no objeto com password encrytada
            val utilizador : Utilizador = Utilizador(editTextEmail.text.toString(), editTextPassword.text.toString())

            // fazer pedido a API e aguardar resultado
            GlobalScope.async {
                val result = Backend.loginUtilizador(utilizador, this@LoginActivity)

                if (result == "ok") {
                    runOnUiThread {
                        Toast.makeText(this@LoginActivity, "Login feito com sucesso!", Toast.LENGTH_LONG).show()

                        // Limpar as editTexts
                        editTextEmail.text.clear()
                        editTextPassword.text.clear()

                        // fazer com que desaparece o dialog
                        dialog.dismiss()

                        // abrir nova activity
                        val intent = Intent(this@LoginActivity, InitialActivity::class.java)
                        startActivity(intent)
                    }
                } else {
                    runOnUiThread {
                        Toast.makeText(this@LoginActivity, "Erro ao fazer login!", Toast.LENGTH_LONG).show()
                        dialog.dismiss()
                    }
                }
            }
        }
    }
}
