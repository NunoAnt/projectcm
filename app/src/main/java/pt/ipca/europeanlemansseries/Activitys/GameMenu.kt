package pt.ipca.europeanlemansseries.Activitys

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import pt.ipca.europeanlemansseries.R

class GameMenu : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_menu)

        val imageViewPlay = findViewById<ImageView>(R.id.imageViewGameMenuPlay)
        val textViewQuit = findViewById<TextView>(R.id.textViewGameMenuQuit)

        imageViewPlay.setOnClickListener {
            val intent = Intent(this@GameMenu, GameActivity::class.java)
            startActivity(intent)
        }

        textViewQuit.setOnClickListener {
            val intent = Intent(this@GameMenu, InitialActivity::class.java)
            startActivity(intent)
        }
    }
}