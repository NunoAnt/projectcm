package pt.ipca.europeanlemansseries.Activitys

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.yqritc.scalablevideoview.ScalableType
import com.yqritc.scalablevideoview.ScalableVideoView
import pt.ipca.europeanlemansseries.R


class MainActivity : AppCompatActivity() {

    private lateinit var buttonRegistar : Button
    private lateinit var buttonLogin : Button
    private lateinit var videoView : ScalableVideoView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        videoView = findViewById(R.id.videoViewBackground)
        buttonRegistar = findViewById(R.id.buttonMainActivityRegistar)
        buttonLogin = findViewById(R.id.buttonMainActivityLogin)

        buttonRegistar.setOnClickListener {
            val intent = Intent(this@MainActivity, RegistarActivity::class.java)
            startActivity(intent)
        }

        buttonLogin.setOnClickListener {
            val intent = Intent(this@MainActivity, LoginActivity::class.java)
            startActivity(intent)
        }

        videoView.setRawData(R.raw.elms_intro)
        videoView.setVolume(1.0f, 1.0f)
        videoView.setScalableType(ScalableType.CENTER_CROP)
        videoView.isLooping = true

        videoView.prepare {
            it.start()
        }
    }

    override fun onPause() {
        super.onPause()

        videoView.stop()
    }
}