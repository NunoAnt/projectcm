package pt.ipca.europeanlemansseries.Activitys

import android.content.Context
import android.content.Intent
import android.graphics.*
import android.os.Build
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.startActivity
import pt.ipca.europeanlemansseries.Models.Game.Car
import pt.ipca.europeanlemansseries.Models.Game.Light
import pt.ipca.europeanlemansseries.R
import java.util.*


class GameView: SurfaceView, Runnable {

    var playing = false
    var gameThread: Thread? = null

    var surfaceHolder: SurfaceHolder? = null
    var canvas: Canvas? = null

    var paint: Paint = Paint()

    var lights = arrayListOf<Light>()
    var cars = arrayListOf<Car>()

    var counterIsOut = 75

    var timer: Int = 0
    var reaction: Double = 0.0

    var click = false

    private fun init(context: Context?, width: Int, height: Int) {
        surfaceHolder = holder

        lights.add(Light(context, width, height, 50, 400))
        lights.add(Light(context, width, height, 250, 400))
        lights.add(Light(context, width, height, 450, 400))
        lights.add(Light(context, width, height, 650, 400))
        lights.add(Light(context, width, height, 850, 400))

        cars.add(Car(context, width, height, -1200, height - 600, BitmapFactory.decodeResource(context?.resources, R.drawable.lmp2_game)))
        cars.add(Car(context, width, height, -2200, height - 600, BitmapFactory.decodeResource(context?.resources, R.drawable.lmp3_game)))
        cars.add(Car(context, width, height, -3200, height - 600, BitmapFactory.decodeResource(context?.resources, R.drawable.lmgte_game)))

        counterIsOut = (75..80).random()
    }

    constructor(context: Context?, width: Int, height: Int) : super(context){
        init(context, width, height)
    }
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs){
        init(context, 0, 0)
    }
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ){
        init(context, 0, 0)
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes){
        init(context, 0, 0)
    }

    override fun run() {
        while (playing){
            update()
            draw()
            control()
        }
    }

    private fun update() {
        if (counterIsOut > 0) {
            when(counterIsOut) {
                13 -> lights[0].isDraw = true
                26 -> lights[1].isDraw = true
                39 -> lights[2].isDraw = true
                52 -> lights[3].isDraw = true
                68 -> lights[4].isDraw = true
            }
            counterIsOut -= 1
        }

        if (counterIsOut == 0 && !click) {
            lights.clear()
            timer += 1

            for (c in cars) {
                c.canMove = true
                c.update()
            }
        }
    }

    private fun draw() {
        surfaceHolder?.let {

            paint.color = Color.WHITE

            if (it.surface.isValid) {
                canvas = surfaceHolder?.lockCanvas()
                val backgroundRect = Rect(0, 0, canvas!!.width, canvas!!.height)
                canvas?.drawBitmap(
                    BitmapFactory.decodeResource(
                        context.resources,
                        R.drawable.bg_jogo
                    ), null, backgroundRect, null
                )

                for (l in lights) {
                    if (l.isDraw)
                        canvas?.drawBitmap(l.bitmap, l.x.toFloat(), l.y.toFloat(), paint)
                }

                for (c in cars)
                    canvas?.drawBitmap(c.bitmap, c.x.toFloat(), c.y.toFloat(), paint)

                paint.color = Color.WHITE
                paint.textSize = 120f
                canvas?.drawText("Timer :${String.format("%.03f", timer / 11.53)}", 50f, 130f, paint)

                if (click) {
                    canvas?.drawColor(Color.BLACK)
                    paint.color = Color.WHITE
                    paint.textSize = 120f
                    canvas?.drawText("Timer :${String.format("%.03f", timer / 11.53)}", width/2f - 350, height/2f, paint)
                }

                surfaceHolder?.unlockCanvasAndPost(canvas)
            }
        }
    }

    private fun control() {
        Thread.sleep(17L)
    }

    fun pause() {
        playing = false
        gameThread?.join()
    }

    fun resume(){
        playing = true
        gameThread = Thread(this)
        gameThread!!.start()
    }


    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_UP -> {

            }
            MotionEvent.ACTION_DOWN -> {
                click = true
                reaction = timer / 11.53
            }
        }

        return true
    }
}