package pt.ipca.europeanlemansseries.Activitys

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.isActive
import pt.ipca.europeanlemansseries.Backend
import pt.ipca.europeanlemansseries.Models.Utilizador
import pt.ipca.europeanlemansseries.R


class RegistarActivity : AppCompatActivity() {

    // Widgets
    private lateinit var editTextNome: EditText
    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var buttonRegistar: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registar)

        editTextNome = findViewById(R.id.editTextRegistarNome)
        editTextEmail = findViewById(R.id.editTextRegistarEmail)
        editTextPassword = findViewById(R.id.editTextRegistarPassword)
        buttonRegistar = findViewById(R.id.buttonLogin)

        buttonRegistar.setOnClickListener {

            // verifica se a textViews estão preenchidas
            if (TextUtils.isEmpty(editTextNome.text)) {
                editTextNome.setError("O nome tem de ser prrenchido")
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(editTextEmail.text)) {
                editTextEmail.setError("O email tem de ser preenchido")
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(editTextPassword.text)) {
                editTextPassword.setError("A password tem de ser prrenchida")
                return@setOnClickListener
            }

            /* Criar Dialog */
            val builder: AlertDialog.Builder = AlertDialog.Builder(this@RegistarActivity)

            val inflater: LayoutInflater = this@RegistarActivity.layoutInflater
            val view = inflater.inflate(R.layout.custom_dialog_loading, null)
            builder.setView(view)

            val imageViewGif = view.findViewById<ImageView>(R.id.imageView3)
            Glide.with(this).load(R.drawable.elms_gift).into(imageViewGif)

            val dialog = builder.create()
            dialog.show() /* Fim */

            // guardar os valores das editTexts no objeto com password encrytada
            val utilizador : Utilizador = Utilizador(0, editTextEmail.text.toString(), editTextNome.text.toString(), editTextPassword.text.toString(), "Portugal")

            // fazer o pedido para guardar e ver qual o resultado
            GlobalScope.async {
                val result = Backend.registarUtilizador(utilizador, this@RegistarActivity)

                if (result == "ok") {
                    runOnUiThread {
                        Toast.makeText(this@RegistarActivity, "Utilizador registado com sucesso!", Toast.LENGTH_LONG).show()

                        // Limpar as editTexts
                        editTextNome.text.clear()
                        editTextEmail.text.clear()
                        editTextPassword.text.clear()

                        dialog.dismiss()
                    }
                } else {
                    runOnUiThread {
                        Toast.makeText(this@RegistarActivity, "Erro ao inserir utilizador!", Toast.LENGTH_LONG).show()
                        dialog.dismiss()
                    }
                }
            }
        }
    }
}