package pt.ipca.europeanlemansseries.Models

import android.content.Context
import android.widget.Toast
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import org.json.JSONException
import org.json.JSONObject
import pt.ipca.europeanlemansseries.Backend
import pt.ipca.europeanlemansseries.Models.Enums.Categoria
import pt.ipca.europeanlemansseries.VolleyHelper.VolleyHelper
import kotlin.coroutines.resume

class Carro {

    var id: Int? = null
    var equipa: Equipa? = null
    var nome: String? = null
    var numero: Int? = null
    var modelo: String? = null
    var fabricante: String? = null
    var marcaPneus: String? = null
    var imagemCarro: String? = null

    constructor(id: Int?, equipa: Equipa?, nome: String?, numero: Int?, modelo: String?, fabricante: String?, marcaPneus: String?, imagemCarro: String?) {
        this.id = id
        this.equipa = equipa
        this.nome = nome
        this.numero = numero
        this.modelo = modelo
        this.fabricante = fabricante
        this.marcaPneus = marcaPneus
        this.imagemCarro = imagemCarro
    }

    constructor(id: Int?, nome: String?, numero: Int?, modelo: String?, fabricante: String?, marcaPneus: String?, imagemCarro: String?) {
        this.id = id
        this.nome = nome
        this.numero = numero
        this.modelo = modelo
        this.fabricante = fabricante
        this.marcaPneus = marcaPneus
        this.imagemCarro = imagemCarro
        this.equipa = Equipa(0, "", Categoria.SEM_CAT, "", "", "", "", "")
    }

    private constructor() {
        this.equipa = Equipa(0, "", Categoria.SEM_CAT, "", "", "", "", "")
    }

    companion object {
        fun fromJson(jsonObject: JSONObject): Carro {
            val carro = Carro()

            try {
                carro.id = jsonObject.getInt("id")
                carro.equipa?.id = jsonObject.getInt("idEquipa")
                carro.nome = jsonObject.getString("nome")
                carro.numero = jsonObject.getInt("numero")
                carro.modelo = jsonObject.getString("modelo")
                carro.fabricante = jsonObject.getString("fabricante")
                carro.marcaPneus = jsonObject.getString("marcaPneus")
                carro.imagemCarro = jsonObject.getString("imagemCarro")
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return carro
        }
    }
}