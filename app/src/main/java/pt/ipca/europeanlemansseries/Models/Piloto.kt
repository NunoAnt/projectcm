package pt.ipca.europeanlemansseries.Models

import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class Piloto {

    var id: Int? = null
    var carro: Carro? = null
    var nome: String? = null
    var idade: Int? = null
    var dataNascimento: Date? = null
    var pais: String? = null
    var bandeiraPais: String? = null
    var facebook: String? = null
    var twitter: String? = null
    var webSite: String? = null
    var foto: String? = null

    constructor(id: Int?, carro: Carro?, nome: String?, idade: Int?, dataNascimento: Date?, pais: String?, bandeiraPais: String?, facebook: String?, twitter: String?, webSite: String?, foto: String?) {
        this.id = id
        this.carro = carro
        this.nome = nome
        this.idade = idade
        this.dataNascimento = dataNascimento
        this.pais = pais
        this.bandeiraPais = bandeiraPais
        this.facebook = facebook
        this.twitter = twitter
        this.webSite = webSite
        this.foto = foto
    }

    constructor(id: Int?, nome: String?, idade: Int?, dataNascimento: Date?, pais: String?, bandeiraPais: String?, facebook: String?, twitter: String?, webSite: String?, foto: String?) {
        this.id = id
        this.carro = Carro(0, "", 0, "", "", "", "")
        this.nome = nome
        this.idade = idade
        this.dataNascimento = dataNascimento
        this.pais = pais
        this.bandeiraPais = bandeiraPais
        this.facebook = facebook
        this.twitter = twitter
        this.webSite = webSite
        this.foto = foto
    }

    private constructor() {
        this.carro = Carro(0, "", 0, "", "", "", "")
    }

    companion object {
        fun fromJson(jsonObject: JSONObject): Piloto {
            val piloto = Piloto()
            val format = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

            try {
                piloto.id = jsonObject.getInt("id")
                piloto.carro?.id = jsonObject.getInt("idCarro")
                piloto.nome = jsonObject.getString("nome")
                piloto.idade = jsonObject.getInt("idade")
                piloto.dataNascimento = format.parse(jsonObject.getString("dataNascimento"))
                piloto.pais = jsonObject.getString("pais")
                piloto.bandeiraPais = jsonObject.getString("bandeiraPais")
                piloto.facebook = jsonObject.getString("facebook")
                piloto.twitter = jsonObject.getString("twitter")
                piloto.webSite = jsonObject.getString("website")
                piloto.foto = jsonObject.getString("foto")
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return piloto
        }
    }
}