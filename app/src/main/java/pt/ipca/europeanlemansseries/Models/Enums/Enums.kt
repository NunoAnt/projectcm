package pt.ipca.europeanlemansseries.Models.Enums

enum class Categoria {
    LMP2, LMP3, GTELM, SEM_CAT
}