package pt.ipca.europeanlemansseries.Models

import org.json.JSONException
import org.json.JSONObject

class Utilizador {

    var id : Int? = null
    var email : String? = null
    var nome : String? = null
    var password :  String? = null
    var pais : String? = null

    constructor(id: Int?, email: String?, nome: String?, password: String?, pais: String?) {
        this.id = id
        this.email = email
        this.nome = nome
        this.password = password
        this.pais = pais
    }

    constructor(email: String?, password: String?) {
        this.email = email
        this.password = password
    }

    private constructor()

    companion object {
        fun fromJson(jsonObject: JSONObject) : Utilizador {
            val utilizador = Utilizador()

            try {
                utilizador.id =       jsonObject.getInt   ("id")
                utilizador.email =    jsonObject.getString("email")
                utilizador.nome =     jsonObject.getString("nome")
                utilizador.password = jsonObject.getString("password")
                utilizador.pais =     jsonObject.getString("pais")
            } catch (e : JSONException) {
                e.printStackTrace()
            }

            return utilizador
        }
    }

    fun toJson() : JSONObject {
        val jsonObject : JSONObject = JSONObject()

        try {
            jsonObject.put("id", id       )
            jsonObject.put("email", email    )
            jsonObject.put("nome", nome     )
            jsonObject.put("password", password )
            jsonObject.put("pais", pais     )
        } catch (e : JSONException) {
            e.printStackTrace()
        }

        return jsonObject
    }
}