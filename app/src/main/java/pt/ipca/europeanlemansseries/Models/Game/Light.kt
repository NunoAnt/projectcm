package pt.ipca.europeanlemansseries.Models.Game

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.text.BoringLayout
import pt.ipca.europeanlemansseries.R

class Light {

    var x = 0
    var y = 0
    var speed = 0
    var maxY = 0
    var minY = 0
    var maxX = 0
    var minX = 0
    var bitmap : Bitmap

    var isDraw: Boolean

    constructor(context: Context?, width: Int, height: Int, x: Int, y: Int){
        bitmap = BitmapFactory.decodeResource(context?.resources, R.drawable.light)

        maxX = width
        minX = 0
        maxY = height
        minY = 0

        this.x = x
        this.y = y

        isDraw = false
    }
}