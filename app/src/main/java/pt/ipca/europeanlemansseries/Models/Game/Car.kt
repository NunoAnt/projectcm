package pt.ipca.europeanlemansseries.Models.Game

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import pt.ipca.europeanlemansseries.R

class Car {

    var x = 0
    var y = 0
    var speed = 0
    var maxY = 0
    var minY = 0
    var maxX = 0
    var minX = 0
    lateinit var bitmap : Bitmap

    var canMove: Boolean

    constructor(context: Context?, width: Int, height: Int, x: Int, y: Int, bitamp: Bitmap){
        bitmap = bitamp

        maxX = width
        minX = 0
        maxY = height
        minY = 0

        speed = 560

        this.x = x
        this.y = y

        canMove = false
    }

    fun update() {
        if (canMove)
            x += speed
    }
}