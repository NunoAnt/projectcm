package pt.ipca.europeanlemansseries.Models

import org.json.JSONException
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class Noticia {

    var id: Int? = null
    var titulo: String? = null
    var thumbnail: String? = null
    var conteudo: String? = null
    var data: Date? = null

    constructor(id: Int?, titulo: String?, thumbnail: String?, conteudo: String?, data: Date?) {
        this.id = id
        this.titulo = titulo
        this.thumbnail = thumbnail
        this.conteudo = conteudo
        this.data = data
    }

    private constructor()

    companion object {
        fun fromJson(jsonObject: JSONObject) : Noticia {
            val noticia = Noticia()
            val format = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

            try {
                noticia.id = jsonObject.getInt("id")
                noticia.titulo = jsonObject.getString("titulo")
                noticia.thumbnail = jsonObject.getString("thumbnail")
                noticia.conteudo = jsonObject.getString("conteudo")
                noticia.data = format.parse(jsonObject.getString("data"))
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return noticia
        }
    }
}