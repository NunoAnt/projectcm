package pt.ipca.europeanlemansseries.Models

import android.content.Context
import org.json.JSONException
import org.json.JSONObject
import pt.ipca.europeanlemansseries.Backend
import pt.ipca.europeanlemansseries.Models.Enums.Categoria

class Equipa {

    var id: Int? = null
    var nome: String? = null
    var categoria: Categoria? = null
    var bandeiraPais: String? = null
    var logo: String? = null
    var facebook: String? = null
    var twitter: String? = null
    var website: String? = null

    lateinit var carros: MutableList<Carro>

    constructor(id: Int?, nome: String?, categoria: Categoria?, bandeiraPais: String?, logo: String?,facebook: String?, twitter: String?, website: String?) {
        this.id = id
        this.nome = nome
        this.categoria = categoria
        this.bandeiraPais = bandeiraPais
        this.logo = logo
        this.facebook = facebook
        this.twitter = twitter
        this.website = website
        this.carros = ArrayList()
    }

    private constructor()

    suspend fun obterCarros(context: Context) {
        carros = Backend.getTeamsCars(context, id!!)
    }

    companion object {
        fun fromJson(jsonObject: JSONObject): Equipa {
            val equipa = Equipa()

            try {
                equipa.id = jsonObject.getInt("id")
                equipa.nome = jsonObject.getString("nome")
                equipa.categoria = checkCategoria(jsonObject.getInt("categoria"))
                equipa.bandeiraPais = jsonObject.getString("bandeiraPais")
                equipa.logo = jsonObject.getString("logo")
                equipa.facebook = jsonObject.getString("facebook")
                equipa.twitter = jsonObject.getString("twitter")
                equipa.website = jsonObject.getString("website")
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return equipa
        }

        fun checkCategoria (categoria: Int): Categoria {
            var categoriaEnum = Categoria.SEM_CAT

            if (categoria == 1)
                categoriaEnum = Categoria.LMP2
            if (categoria == 2)
                categoriaEnum = Categoria.LMP3
            if (categoria == 3)
                categoriaEnum = Categoria.GTELM

            return categoriaEnum
        }

        fun setCategoria (categoria: Categoria): Int {
            var categoriaEnum = 0

            if (categoria == Categoria.LMP2)
                categoriaEnum = 1
            if (categoria == Categoria.LMP3)
                categoriaEnum = 2
            if (categoria == Categoria.GTELM)
                categoriaEnum = 3
            if (categoria == Categoria.SEM_CAT)
                categoriaEnum = 0

            return categoriaEnum
        }
    }
}