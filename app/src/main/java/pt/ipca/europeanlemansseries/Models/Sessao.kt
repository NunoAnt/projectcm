package pt.ipca.europeanlemansseries.Models

import android.util.Log
import org.json.JSONException
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class Sessao {

    var id: Int? = null
    var nome: String? = null
    var dia: Date? = null
    var horaInicio: Date? = null
    var horaFim: Date? = null
    var corrida: Corrida? = null

    constructor(id: Int?, nome: String?, dia: Date?, horaInicio: Date?, horaFim: Date?, corrida: Corrida) {
        this.id = id
        this.nome = nome
        this.dia = dia
        this.horaInicio = horaInicio
        this.horaFim = horaFim
        this.corrida = corrida
    }

    private constructor() {
        this.corrida = Corrida(0, "", "", "", 0.00, 0, "", "", "", "", "", "", Date(), Date())
    }

    companion object {
        fun fromJson(jsonObject: JSONObject): Sessao {
            val sessao = Sessao()
            val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())

            try {
                sessao.id = jsonObject.getInt("id")
                sessao.nome = jsonObject.getString("nome")
                sessao.dia = format.parse(jsonObject.getString("dia"))
                sessao.horaInicio = format.parse(jsonObject.getString("horaInicio"))
                sessao.horaFim = format.parse(jsonObject.getString("horaFim"))
                sessao.corrida?.id = jsonObject.getInt("idCorrida")
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return sessao
        }
    }
}