package pt.ipca.europeanlemansseries.Models

import org.json.JSONException
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

class Corrida {

    var id: Int? = null
    var nome: String? = null
    var pais: String? = null
    var bandeiraPais: String? = null
    var distancia: Double? = null
    var curvas: Int? = null
    var morada: String? = null
    var telefone: String? = null
    var website: String? = null
    var foto: String? = null
    var trackLayout: String? = null
    var replay: String? = null
    var dataInicio: Date? = null
    var dataFim: Date? = null

    constructor(
        id: Int?,
        nome: String?,
        pais: String?,
        bandeiraPais: String?,
        distancia: Double?,
        curvas: Int?,
        morada: String?,
        telefone: String?,
        website: String?,
        foto: String?,
        trackLayout: String?,
        replay: String?,
        dataInicio: Date?,
        dataFim: Date?
    ) {
        this.id = id
        this.nome = nome
        this.pais = pais
        this.bandeiraPais = bandeiraPais
        this.distancia = distancia
        this.curvas = curvas
        this.morada = morada
        this.telefone = telefone
        this.website = website
        this.foto = foto
        this.trackLayout = trackLayout
        this.replay = replay
        this.dataInicio = dataInicio
        this.dataFim = dataFim
    }

    private constructor()

    companion object {
        fun fromJson(jsonObject: JSONObject) : Corrida {
            val corrida = Corrida()
            val format = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

            try {
                corrida.id = jsonObject.getInt("id")
                corrida.nome = jsonObject.getString("nome")
                corrida.pais = jsonObject.getString("pais")
                corrida.bandeiraPais = jsonObject.getString("bandeiraPais")
                corrida.distancia = jsonObject.getDouble("distancia")
                corrida.curvas = jsonObject.getInt("numeroCurvas")
                corrida.morada = jsonObject.getString("morada")
                corrida.telefone = jsonObject.getString("telefone")
                corrida.website = jsonObject.getString("website")
                corrida.foto = jsonObject.getString("foto")
                corrida.trackLayout = jsonObject.getString("trackLayout")
                corrida.replay = jsonObject.getString("replay")
                corrida.dataInicio = format.parse(jsonObject.getString("dataInicio"))
                corrida.dataFim = format.parse(jsonObject.getString("dataFim"))
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return corrida
        }
    }
}