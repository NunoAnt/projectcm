package pt.ipca.europeanlemansseries.Models

import org.json.JSONException
import org.json.JSONObject
import pt.ipca.europeanlemansseries.Models.Enums.Categoria
import java.text.ParseException

class Classificacao {

    var carro: Carro? = null
    var equipa: Equipa? = null
    var pontos: Float? = null
    var categoria: Categoria? = null

    constructor(carro: Carro?, equipa: Equipa?, pontos: Float?, categoria: Categoria?) {
        this.carro = carro
        this.equipa = equipa
        this.pontos = pontos
        this.categoria = categoria
    }

    private constructor()

    companion object {
        fun fromJson(jsonObject: JSONObject): Classificacao {
            val classificacao = Classificacao()

            try {
                classificacao.carro = Carro.fromJson(jsonObject.getJSONObject("carro"))
                classificacao.equipa = Equipa.fromJson(jsonObject.getJSONObject("equipa"))
                classificacao.pontos = jsonObject.getDouble("pontos").toFloat()
                classificacao.categoria = checkCategoria(jsonObject.getInt("categoria"))
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return classificacao
        }

        fun checkCategoria (categoria: Int): Categoria {
            var categoriaEnum = Categoria.SEM_CAT

            if (categoria == 1)
                categoriaEnum = Categoria.LMP2
            if (categoria == 2)
                categoriaEnum = Categoria.LMP3
            if (categoria == 3)
                categoriaEnum = Categoria.GTELM

            return categoriaEnum
        }
    }
}