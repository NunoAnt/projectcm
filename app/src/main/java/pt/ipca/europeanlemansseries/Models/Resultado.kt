package pt.ipca.europeanlemansseries.Models

import org.json.JSONException
import org.json.JSONObject
import pt.ipca.europeanlemansseries.Models.Enums.Categoria
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class Resultado {
    var id: Int? = null
    var sessao: Sessao? = null
    var carro: Carro? = null
    var equipa: Equipa? = null
    var tipo: Int? = null
    var pilotos: MutableList<Piloto>? = null
    var classificacao: Int? = null
    var categoria: Categoria? = null

    constructor(id: Int?, sessao: Sessao?, carro: Carro?, tipo: Int?, pilotos: MutableList<Piloto>?, classificacao: Int?, categoria: Categoria?) {
        this.id = id
        this.sessao = sessao
        this.carro = carro
        this.tipo = tipo
        this.pilotos = pilotos
        this.classificacao = classificacao
        this.categoria = categoria
    }

    private constructor()

    companion object {
        fun fromJson(jsonObject: JSONObject): Resultado {
            val resultado = Resultado()
            var pilotos: MutableList<Piloto> = ArrayList()

            try {
                resultado.id = jsonObject.getInt("id")
                resultado.sessao = Sessao.fromJson(jsonObject.getJSONObject("sessao"))
                resultado.carro = Carro.fromJson(jsonObject.getJSONObject("carro"))
                resultado.equipa = Equipa.fromJson(jsonObject.getJSONObject("equipa"))
                resultado.tipo = jsonObject.getInt("tipo")
                for (i in 0 until jsonObject.getJSONArray("pilotos").length()){
                    val p = Piloto.fromJson(jsonObject.getJSONArray("pilotos").getJSONObject(i))
                    pilotos.add(p)
                }
                resultado.pilotos = pilotos
                resultado.classificacao = jsonObject.getInt("classificacao")
                resultado.categoria = checkCategoria(jsonObject.getInt("categoria"))
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return resultado
        }

        fun checkCategoria (categoria: Int): Categoria {
            var categoriaEnum = Categoria.SEM_CAT

            if (categoria == 1)
                categoriaEnum = Categoria.LMP2
            if (categoria == 2)
                categoriaEnum = Categoria.LMP3
            if (categoria == 3)
                categoriaEnum = Categoria.GTELM

            return categoriaEnum
        }
    }
}