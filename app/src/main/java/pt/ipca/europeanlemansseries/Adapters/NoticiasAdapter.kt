package pt.ipca.europeanlemansseries.Adapters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import pt.ipca.europeanlemansseries.Fragments.NoticiasDetailFragment
import pt.ipca.europeanlemansseries.Models.Noticia
import pt.ipca.europeanlemansseries.R
import java.text.SimpleDateFormat
import java.util.*

class NoticiasAdapter(private val noticias: List<Noticia>, private val context: Context, private val fragmentManager: FragmentManager) : RecyclerView.Adapter<NoticiasAdapter.NoticasHomeViewHolder>() {

    inner class NoticasHomeViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal val imageViewThumbnail = view.findViewById<ImageView>(R.id.imageViewItemRowNoticias)
        internal val textViewData = view.findViewById<TextView>(R.id.textViewItemRowNoticiasData)
        internal val textViewNomeTitutlo = view.findViewById<TextView>(R.id.textViewItemRowNiticiasTitulo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoticasHomeViewHolder {
        return NoticasHomeViewHolder(
                LayoutInflater.from(parent.context).inflate(
                        R.layout.item_row_noticias,
                        parent,
                        false
                )
        )
    }

    override fun onBindViewHolder(holder: NoticasHomeViewHolder, position: Int) {
        val noticia: Noticia = noticias[position]

        val format = SimpleDateFormat("EEE, MMM dd", Locale.getDefault())

        Picasso.get().load(noticia.thumbnail).fit().centerCrop().error(R.drawable.logo_elms).into(holder.imageViewThumbnail)
        holder.textViewData.text = format.format(noticia.data)
        if (noticia.titulo.toString().length >= 30) {
            holder.textViewNomeTitutlo.text = noticia.titulo?.take(29)
            holder.textViewNomeTitutlo.text = holder.textViewNomeTitutlo.text as String + "..."
        } else
            holder.textViewNomeTitutlo.text = noticia.titulo

        holder.imageViewThumbnail.setOnClickListener {
            val noticiasDetailFragment = NoticiasDetailFragment()
            val args = Bundle()
            args.putInt(NoticiasDetailFragment.NOTICIA_ID, noticias[position].id!!)
            args.putString(NoticiasDetailFragment.NOTICIA_TITULO, noticias[position].titulo!!)
            args.putString(NoticiasDetailFragment.NOTICIA_THUMBNAIL, noticias[position].thumbnail!!)
            args.putString(NoticiasDetailFragment.NOTICIA_CONTEUDO, noticias[position].conteudo!!)
            args.putLong(NoticiasDetailFragment.NOTICIA_DATA, noticias[position].data?.time!!)
            noticiasDetailFragment.arguments = args

            val transaction = this.fragmentManager
            transaction.beginTransaction().replace(R.id.nav_host_fragment, noticiasDetailFragment).setTransition(
                    FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()
        }
    }

    override fun getItemCount(): Int {
        return noticias.size
    }
}