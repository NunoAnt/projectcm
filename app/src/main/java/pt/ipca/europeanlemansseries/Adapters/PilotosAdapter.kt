package pt.ipca.europeanlemansseries.Adapters

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import pt.ipca.europeanlemansseries.Fragments.PilotoDetailFragment
import pt.ipca.europeanlemansseries.Models.Piloto
import pt.ipca.europeanlemansseries.R
import java.text.SimpleDateFormat
import java.util.*

class PilotosAdapter: RecyclerView.Adapter<PilotosAdapter.PilotosViewHolder> {

    var pilotos: MutableList<Piloto>
    var context: Context
    var fragmentManager: FragmentManager

    constructor(pilotos: MutableList<Piloto>, context: Context, fragmentManager: FragmentManager) {
        this.pilotos = pilotos
        this.context = context
        this.fragmentManager = fragmentManager
    }

    inner class PilotosViewHolder(view: View): RecyclerView.ViewHolder(view) {
        internal val imageViewFoto = view.findViewById<ImageView>(R.id.imageViewItemGridPilotoFoto)
        internal val textViewNomePiloto = view.findViewById<TextView>(R.id.textViewItemGridPilotosNome)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PilotosViewHolder {
        return PilotosViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_grid_pilotos,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: PilotosViewHolder, position: Int) {
        val piloto: Piloto = pilotos[position]

        Picasso.get().load(piloto.foto).fit().centerInside().error(R.drawable.logo_elms).into(holder.imageViewFoto)
        holder.textViewNomePiloto.text = piloto.nome

        holder.itemView.isClickable = true
        holder.itemView.setOnClickListener {
            val detailPilotoFragment = PilotoDetailFragment()
            val args = Bundle()
            pilotos[position]?.id?.let { it1 ->
                args.putInt(PilotoDetailFragment.PILOTO_ID,
                        it1
                )
            }
            pilotos[position].carro?.id?.let { it1 ->
                args.putInt(PilotoDetailFragment.PILOTO_CARRO_ID,
                        it1
                )
            }
            args.putString(PilotoDetailFragment.PILOTO_NOME, pilotos[position].nome)
            pilotos[position].idade?.let { it1 ->
                args.putInt(PilotoDetailFragment.PILOTO_IDADE,
                        it1
                )
            }
            pilotos[position].dataNascimento?.time?.let { it1 ->
                args.putLong(PilotoDetailFragment.PILOTO_DATA_NASCIMENTO,
                        it1
                )
            }
            args.putString(PilotoDetailFragment.PILOTO_PAIS, pilotos[position].pais)
            args.putString(PilotoDetailFragment.PILOTO_BANDEIRA_PAIS, pilotos[position].bandeiraPais)
            args.putString(PilotoDetailFragment.PILOTO_FACEBOOK, pilotos[position].facebook)
            args.putString(PilotoDetailFragment.PILOTO_TWITTER, pilotos[position].twitter)
            args.putString(PilotoDetailFragment.PILOTO_WEBSITE, pilotos[position].webSite)
            args.putString(PilotoDetailFragment.PILOTO_FOTO, pilotos[position].foto)
            detailPilotoFragment.arguments = args

            val transaction = fragmentManager
            transaction.beginTransaction().replace(R.id.nav_host_fragment, detailPilotoFragment).setTransition(
                    FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()
        }
    }

    override fun getItemCount(): Int {
        return pilotos.size
    }
}