package pt.ipca.europeanlemansseries.Adapters

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import pt.ipca.europeanlemansseries.Fragments.ClassificacaoFragment
import pt.ipca.europeanlemansseries.Fragments.CorridaDetailFragment
import pt.ipca.europeanlemansseries.Models.Corrida
import pt.ipca.europeanlemansseries.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.abs

class SlideHomeAdapter(val fragmentManager: FragmentManager, private val corridas: List<Corrida>) :
    RecyclerView.Adapter<SlideHomeAdapter.SliderHomeViewHolder>() {

    inner class SliderHomeViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val textViewDia = view.findViewById<TextView>(R.id.textViewSliderHomeDia)
        private val textViewMes = view.findViewById<TextView>(R.id.textViewSliderHomeMes)
        private val imageViewPais = view.findViewById<ImageView>(R.id.imageViewSliderHomeBandeiraPais)
        private val textViewNomePais = view.findViewById<TextView>(R.id.textViewSliderHomeNomePais)
        private val textViewNomeCorrida = view.findViewById<TextView>(R.id.textViewSliderHomeNomeCorrida)
        private val textViewDataInicioCorrida = view.findViewById<TextView>(R.id.textViewSliderHomeDataInicio)
        private val textViewDataFimCorrida = view.findViewById<TextView>(R.id.textViewSliderHomeDataFim)
        private val textViewClassificacoes = view.findViewById<TextView>(R.id.textViewSliderHomeClassificacao)
        private val textViewResultado = view.findViewById<TextView>(R.id.textViewSliderHomeResultado)

        fun bind(corrida: Corrida, fragmentManager: FragmentManager) {
            val format = SimpleDateFormat("EEE MMM dd", Locale.getDefault())
            val formatDia = SimpleDateFormat("dd", Locale.getDefault())
            val formatMes = SimpleDateFormat("MMM", Locale.getDefault())

            textViewDia.text = formatDia.format(corrida.dataFim)
            textViewMes.text = formatMes.format(corrida.dataFim).toUpperCase()
            Picasso.get().load(corrida.bandeiraPais).resize(/*Equals Math.abs */abs(imageViewPais.measuredWidth.let { 50 }), abs(imageViewPais.measuredHeight.let { 50 })).centerCrop().error(R.drawable.logo_elms).into(imageViewPais)
            textViewNomePais.text = corrida.pais
            textViewNomeCorrida.text = corrida.nome
            textViewDataInicioCorrida.text = format.format(corrida.dataInicio)
            textViewDataFimCorrida.text = format.format(corrida.dataFim)
            textViewClassificacoes.text = "Standings"
            textViewResultado.text = "Result"

            textViewClassificacoes.setOnClickListener {
                val fragmentClassificacao = ClassificacaoFragment()

                val transaction = fragmentManager
                transaction.beginTransaction().replace(R.id.nav_host_fragment, fragmentClassificacao).setTransition(
                        FragmentTransaction.TRANSIT_FRAGMENT_FADE
                ).commit()
            }

            textViewResultado.setOnClickListener {
                val corridaFragment = CorridaDetailFragment()
                val args = Bundle()
                corrida.id?.let { it1 ->
                    args.putInt(CorridaDetailFragment.CORRIDA_ID,
                            it1
                    )
                }
                args.putString(CorridaDetailFragment.CORRIDA_NOME, corrida.nome)
                args.putString(CorridaDetailFragment.CORRIDA_PAIS, corrida.pais)
                args.putString(CorridaDetailFragment.CORRIDA_BANDEIRA_PAIS, corrida.bandeiraPais)
                corrida.distancia?.let { it1 ->
                    args.putDouble(CorridaDetailFragment.CORRIDA_DISTANCIA,
                            it1
                    )
                }
                corrida.curvas?.let { it1 ->
                    args.putInt(CorridaDetailFragment.CORRIDA_CURVAS,
                            it1
                    )
                }
                args.putString(CorridaDetailFragment.CORRIDA_MORADA, corrida.morada)
                args.putString(CorridaDetailFragment.CORRIDA_TELEFONE, corrida.telefone)
                args.putString(CorridaDetailFragment.CORRIDA_WEBSITE, corrida.website)
                args.putString(CorridaDetailFragment.CORRIDA_FOTO, corrida.foto)
                args.putString(CorridaDetailFragment.CORRIDA_TRACK_LAYOUT, corrida.trackLayout)
                args.putString(CorridaDetailFragment.CORRIDA_REPLAY, corrida.replay)
                corrida.dataInicio?.time?.let { it1 ->
                    args.putLong(CorridaDetailFragment.CORRIDA_DATA_INICIO,
                            it1
                    )
                }
                corrida.dataFim?.time?.let { it1 ->
                    args.putLong(CorridaDetailFragment.CORRIDA_DATA_FIM,
                            it1
                    )
                }
                corridaFragment.arguments = args

                val transaction = fragmentManager
                transaction.beginTransaction().replace(R.id.nav_host_fragment, corridaFragment).setTransition(
                        FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SliderHomeViewHolder {
        return SliderHomeViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.slideshow_home_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SliderHomeViewHolder, position: Int) {
        holder.bind(corridas[position], fragmentManager)
    }

    override fun getItemCount(): Int {
        return corridas.size
    }
}