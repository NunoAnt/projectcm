package pt.ipca.europeanlemansseries.Adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import pt.ipca.europeanlemansseries.Fragments.CarroDetailFragment
import pt.ipca.europeanlemansseries.Models.Carro
import pt.ipca.europeanlemansseries.R

class SliderCarsAdapter : RecyclerView.Adapter<SliderCarsAdapter.SliderCarsViewHolder> {

    var carros: MutableList<Carro>
    var fragmentManager: FragmentManager

    constructor(carros: MutableList<Carro>, fragmentManager: FragmentManager) {
        this.carros = carros
        this.fragmentManager = fragmentManager
    }

    inner class SliderCarsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val imageViewCarro = view.findViewById<ImageView>(R.id.imageViewSliderCarro)
        internal val textViewNome  = view.findViewById<TextView>(R.id.textViewSliderCarroNome)

        fun bind (carro: Carro) {
            Picasso.get().load(carro.imagemCarro).fit().centerInside().error(R.drawable.logo_elms).into(imageViewCarro)
            textViewNome.text = "#".plus(carro.numero.toString()).plus(" ").plus(carro.modelo)

            imageViewCarro.setOnClickListener {
                var carroDetailFragment = CarroDetailFragment()
                val args = Bundle()
                carro.id?.let { it1 ->
                    args.putInt(
                        CarroDetailFragment.CARRO_ID,
                        it1
                    )
                }
                carro.equipa?.id?.let { it1 ->
                    args.putInt(
                        CarroDetailFragment.CARRO_ID_EQUIPA,
                        it1
                    )
                }
                args.putString(CarroDetailFragment.CARRO_NOME, carros[position].nome)
                carro.numero?.let { it1 ->
                    args.putInt(
                        CarroDetailFragment.CARRO_NUMERO,
                        it1
                    )
                }
                args.putString(CarroDetailFragment.CARRO_MODELO, carro.modelo)
                args.putString(CarroDetailFragment.CARRO_FABRICANTE, carro.fabricante)
                args.putString(CarroDetailFragment.CARRO_MARCA_PNEUS, carro.marcaPneus)
                args.putString(CarroDetailFragment.CARRO_IMAGEM_CARRO, carro.imagemCarro)
                carroDetailFragment.arguments = args


                val transaction = fragmentManager
                transaction.beginTransaction().replace(R.id.nav_host_fragment, carroDetailFragment).setTransition(
                    FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SliderCarsViewHolder {
        return SliderCarsViewHolder(
                LayoutInflater.from(parent.context).inflate(
                        R.layout.slider_cars,
                        parent,
                        false
                )
        )
    }

    override fun onBindViewHolder(holder: SliderCarsViewHolder, position: Int) {
        holder.bind(carros[position])
    }

    override fun getItemCount(): Int {
        return carros.size
    }
}