package pt.ipca.europeanlemansseries.Fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import pt.ipca.europeanlemansseries.Adapters.NoticiasAdapter
import pt.ipca.europeanlemansseries.Adapters.SlideHomeAdapter
import pt.ipca.europeanlemansseries.Backend
import pt.ipca.europeanlemansseries.Models.Corrida
import pt.ipca.europeanlemansseries.Models.Noticia
import pt.ipca.europeanlemansseries.R
import java.util.*
import kotlin.collections.ArrayList


class HomeFragment : Fragment() {

    // slider corridas
    lateinit var sliderHomeAdapter: SlideHomeAdapter
    var corridas : MutableList<Corrida> = ArrayList()

    // lista nóticias
    lateinit var recyclerViewNoticias: RecyclerView
    var noticias: MutableList<Noticia> = ArrayList()
    lateinit var noticiasAdapter: NoticiasAdapter

    lateinit var rootView: View
    lateinit var viewPagerHome: ViewPager2
    lateinit var indicatorsContainer: LinearLayout
    lateinit var textViewLastRace: TextView
    lateinit var linearLayoutLastRace: LinearLayout


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_home, container, false)
        setHasOptionsMenu(true)

        viewPagerHome = rootView.findViewById(R.id.viewPagerIdHome)
        indicatorsContainer = rootView.findViewById(R.id.linearLayoutHomeIndicators)
        recyclerViewNoticias = rootView.findViewById(R.id.recyclerViewHomeNoticias)
        textViewLastRace = rootView.findViewById(R.id.textViewHomeLastRace)
        linearLayoutLastRace = rootView.findViewById(R.id.linearLayoutHomeLastRaceReplau)

        // criar slider
        sliderHomeAdapter = SlideHomeAdapter(this.parentFragmentManager, corridas)
        viewPagerHome.adapter = sliderHomeAdapter

        // fazer pedido a API e aguardar resultado
        GlobalScope.async {
            corridas = Backend.getAllRaces(this@HomeFragment.requireContext())

            if (corridas.size == 0) {
                this@HomeFragment.activity?.runOnUiThread {
                    Toast.makeText(
                        this@HomeFragment.requireContext(),
                        "Não há corridas!",
                        Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                this@HomeFragment.activity?.runOnUiThread {
                    sliderHomeAdapter = SlideHomeAdapter(this@HomeFragment.parentFragmentManager, corridas)
                    viewPagerHome.adapter = sliderHomeAdapter
                    sliderHomeAdapter.notifyDataSetChanged()

                    // alterar o titulo da textView da ultima corrida
                    if (corridas.size == 0) {
                        textViewLastRace.text = ""
                    } else {
                        textViewLastRace.text = corridas.last().nome
                    }
                }
            }
        }

        // criar slider
        sliderHomeAdapter = SlideHomeAdapter(this@HomeFragment.parentFragmentManager, corridas)
        viewPagerHome.adapter = sliderHomeAdapter
        sliderHomeAdapter.notifyDataSetChanged()

        // atualizar os indicadores do slider
        setUpIndicators()
        viewPagerHome.registerOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {
            override fun onPageScrollStateChanged(position: Int) {
                super.onPageScrollStateChanged(position)
                setCurrentIndicator(position)
            }
        })


        noticiasAdapter = NoticiasAdapter(noticias, requireActivity().applicationContext, this@HomeFragment.parentFragmentManager)

        val layoutManager = LinearLayoutManager(
            this@HomeFragment.requireContext(),
            LinearLayoutManager.HORIZONTAL,
            false
        )
        recyclerViewNoticias.layoutManager = layoutManager
        noticiasAdapter.notifyDataSetChanged()
        recyclerViewNoticias.adapter = noticiasAdapter

        // fazer pedido a API
        GlobalScope.async {
            noticias = Backend.getLastTreeNotices(this@HomeFragment.requireContext())

            if (noticias.size == 0) {
                this@HomeFragment.activity?.runOnUiThread {
                    Toast.makeText(
                            this@HomeFragment.requireContext(),
                            "Não há notícias!",
                            Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                this@HomeFragment.activity?.runOnUiThread {
                    noticiasAdapter = NoticiasAdapter(noticias, requireActivity().applicationContext, this@HomeFragment.parentFragmentManager)

                    val layoutManager = LinearLayoutManager(
                            this@HomeFragment.requireContext(),
                            LinearLayoutManager.HORIZONTAL,
                            false
                    )
                    recyclerViewNoticias.layoutManager = layoutManager
                    noticiasAdapter.notifyDataSetChanged()
                    recyclerViewNoticias.adapter = noticiasAdapter

                    noticiasAdapter.notifyDataSetChanged()
                }
            }
        }

        // abrir o ultimo replay
        linearLayoutLastRace.setOnClickListener {
            val replayCorridaFragment = ReplayCorridaFragment()
            val args = Bundle()
            args.putString(ReplayCorridaFragment.REPLAY_ID, corridas.last().replay)
            args.putString(ReplayCorridaFragment.REPLAY_TITULO, corridas.last().nome)
            replayCorridaFragment.arguments = args

            val transaction = this@HomeFragment.parentFragmentManager
            transaction.beginTransaction().replace(R.id.nav_host_fragment, replayCorridaFragment).setTransition(
                    FragmentTransaction.TRANSIT_FRAGMENT_FADE
            ).commit()
        }

        return rootView
    }

    private fun setUpIndicators() {
        val indicators = kotlin.arrayOfNulls<ImageView>(sliderHomeAdapter.itemCount)
        val layoutParams: LinearLayout.LayoutParams =
            LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
        layoutParams.setMargins(8, 0, 8, 0)
        for (i in indicators.indices) {
            indicators[i] = ImageView(activity?.applicationContext)
            indicators[i].apply {
                this?.setImageDrawable(
                    activity?.applicationContext?.let {
                        ContextCompat.getDrawable(
                            it,
                            R.drawable.indicator_inactive
                        )
                    }
                )
                this?.layoutParams = layoutParams
            }
            indicatorsContainer.addView(indicators[i])
        }
    }

    private fun setCurrentIndicator(index: Int) {
        val childCount = indicatorsContainer.childCount
        for (i in 0 until childCount) {
            val imageView = indicatorsContainer[i] as ImageView
            if (i == index) {
                imageView.setImageDrawable(
                    activity?.applicationContext?.let {
                        ContextCompat.getDrawable(
                            it,
                            R.drawable.indicator_active
                        )
                    }
                )
            } else {
                imageView.setImageDrawable(
                    activity?.applicationContext?.let {
                        ContextCompat.getDrawable(
                            it,
                            R.drawable.indicator_inactive
                        )
                    }
                )
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.initail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_total -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.total.pt/"))
                startActivity(browserIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}