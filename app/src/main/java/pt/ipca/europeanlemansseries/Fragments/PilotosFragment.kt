package pt.ipca.europeanlemansseries.Fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import pt.ipca.europeanlemansseries.Backend
import pt.ipca.europeanlemansseries.Models.Piloto
import pt.ipca.europeanlemansseries.R

class PilotosFragment : Fragment() {

    // Widgets
    lateinit var rootView: View
    lateinit var listViewPilotos: ListView
    lateinit var editTextPilotoSearch: TextView

    var pilotos: MutableList<Piloto> = ArrayList()
    var pilotosSearch: MutableList<Piloto> = ArrayList()
    lateinit var adapterPilotos: PilotosAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_pilotos, container, false)
        setHasOptionsMenu(true)

        listViewPilotos = rootView.findViewById(R.id.listViewPilotos)
        editTextPilotoSearch = rootView.findViewById(R.id.editTextPilotosSearch)

        adapterPilotos = PilotosAdapter()
        listViewPilotos.adapter = adapterPilotos
        adapterPilotos.notifyDataSetChanged()

        GlobalScope.async {
            pilotos = Backend.getDrivers(this@PilotosFragment.requireContext())

            if (pilotos.size == 0) {
                this@PilotosFragment.activity?.runOnUiThread {
                    Toast.makeText(
                            this@PilotosFragment.requireContext(),
                            "Não há pilotos!",
                            Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                this@PilotosFragment.activity?.runOnUiThread {
                    pilotosSearch = pilotos
                    adapterPilotos.notifyDataSetChanged()
                }
            }
        }

        // Procurar na Lista
        editTextPilotoSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                filter(editTextPilotoSearch.text.toString())
            }

        })


        return rootView
    }

    private fun filter(text: String) {
        pilotosSearch = pilotos.filter {
            piloto ->  piloto.nome?.toLowerCase()?.contains(text.toLowerCase()) == true
        } as MutableList<Piloto>
        adapterPilotos.notifyDataSetChanged()
    }


    inner class PilotosAdapter : BaseAdapter() {
        override fun getCount(): Int {
             return pilotosSearch.size
        }

        override fun getItem(position: Int): Any {
            return pilotosSearch[position]
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val rowView = layoutInflater.inflate(R.layout.item_row_pilotos, parent, false)
            val textViewNomePiloto = rowView.findViewById<TextView>(R.id.textViewItemRowPilotosNome)

            textViewNomePiloto.text = pilotosSearch[position].nome

            rowView.isClickable = true
            rowView.setOnClickListener {
                val detailPilotoFragment = PilotoDetailFragment()
                val args = Bundle()
                pilotosSearch[position]?.id?.let { it1 ->
                    args.putInt(PilotoDetailFragment.PILOTO_ID,
                            it1
                    )
                }
                pilotosSearch[position].carro?.id?.let { it1 ->
                    args.putInt(PilotoDetailFragment.PILOTO_CARRO_ID,
                            it1
                    )
                }
                args.putString(PilotoDetailFragment.PILOTO_NOME, pilotosSearch[position].nome)
                pilotosSearch[position].idade?.let { it1 ->
                    args.putInt(PilotoDetailFragment.PILOTO_IDADE,
                            it1
                    )
                }
                pilotosSearch[position].dataNascimento?.time?.let { it1 ->
                    args.putLong(PilotoDetailFragment.PILOTO_DATA_NASCIMENTO,
                            it1
                    )
                }
                args.putString(PilotoDetailFragment.PILOTO_PAIS, pilotosSearch[position].pais)
                args.putString(PilotoDetailFragment.PILOTO_BANDEIRA_PAIS, pilotosSearch[position].bandeiraPais)
                args.putString(PilotoDetailFragment.PILOTO_FACEBOOK, pilotosSearch[position].facebook)
                args.putString(PilotoDetailFragment.PILOTO_TWITTER, pilotosSearch[position].twitter)
                args.putString(PilotoDetailFragment.PILOTO_WEBSITE, pilotosSearch[position].webSite)
                args.putString(PilotoDetailFragment.PILOTO_FOTO, pilotosSearch[position].foto)
                detailPilotoFragment.arguments = args

                val transaction = this@PilotosFragment.parentFragmentManager
                transaction.beginTransaction().replace(R.id.nav_host_fragment, detailPilotoFragment).setTransition(
                        FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()
            }

            return rowView
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.initail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_total -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.total.pt/"))
                startActivity(browserIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}