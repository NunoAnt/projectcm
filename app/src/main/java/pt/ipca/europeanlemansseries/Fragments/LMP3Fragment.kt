package pt.ipca.europeanlemansseries.Fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.TextView
import pt.ipca.europeanlemansseries.R


class LMP3Fragment : Fragment() {

    lateinit var rootView: View
    lateinit var textViewAboutPart1: TextView
    lateinit var textViewAboutPart2: TextView
    lateinit var textViewAboutPart3: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_l_m_p3, container, false)
        setHasOptionsMenu(true)

        textViewAboutPart1 = rootView.findViewById(R.id.textViewAboutLmp3Part1)
        textViewAboutPart2 = rootView.findViewById(R.id.textViewAboutLmp3Part2)
        textViewAboutPart3 = rootView.findViewById(R.id.textViewAboutLmp3Part3)

        textViewAboutPart1.text = "The LMP3 class was introduced by the Automobile Club de l’Ouest (ACO) in 2015 and 2020 will mark the introduction of the new generation of LMP3. Four manufacturers have been selected to help the ACO take the class forward: Ligier, Duqueine Engineering, Ginetta and Adess.\n\n" +
                "Competitive cars, a top-flight sporting programme, an affordable budget, a straightforward mechanical set-up and technology within everyone’s reach will continue to form the backbone of the series. LMP3 was designed as a training ground for endurance racing, an arena in which drivers, team members, engineers and mechanics can hone their skills and prepare for the 24 Hours of Le Mans and the FIA World Endurance Championship."

        textViewAboutPart2.text = "- Oreca is the exclusive powertrain (engine, gearbox and electronics) supplier\n\n" +
                "- The car will be an upgrade of the manufacturers’ present vehicles, not an entirely new car. An upgrade kit has been defined, covering safety, performance and design\n\n" +
                "- Customer quality will be a prime concern with technical support at circuits, a spare parts service and a technical passport"

        textViewAboutPart3.text = "- Engine power has been increased by 35 hp, to 455 hp\n\n" +
                "- The new Nissan VK56 engine will be introduced. The current engines (Nissan VK50 V8) will be adapted to deliver the same power and performance as the VK56. Competitors can thus keep their current engines until they come to the end of their useful life and then acquire the new one\n\n" +
                " - As for the chassis, some safety-related changes have been approved for the driver’s headrest structure and the driver’s seat, and Zylon side panels will be introduced.\n\n" +
                "- Introduction of traction control\n\n" +
                "Race Number Background Colour: PURPLE."

        return rootView
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.initail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_total -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.total.pt/"))
                startActivity(browserIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}