package pt.ipca.europeanlemansseries.Fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.LinearLayout
import androidx.fragment.app.FragmentTransaction
import pt.ipca.europeanlemansseries.R

class CompetitorsFragment : Fragment() {

    // Widgets
    lateinit var roowView: View
    lateinit var linearLayoutEquipas: LinearLayout
    lateinit var linearLayoutCarros: LinearLayout
    lateinit var linearLayoutPilotos: LinearLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        roowView = inflater.inflate(R.layout.fragment_competitors, container, false)
        setHasOptionsMenu(true)

        linearLayoutEquipas = roowView.findViewById(R.id.linearLayoutCompetitoresEquipas)
        linearLayoutCarros = roowView.findViewById(R.id.linearLayoutCompetitoresCarros)
        linearLayoutPilotos = roowView.findViewById(R.id.linearLayoutCompetitoresPilotos)

        linearLayoutEquipas.setOnClickListener {
            val equipasFragment = EquipasFragment()

            val transaction = this.parentFragmentManager
            transaction.beginTransaction().replace(R.id.nav_host_fragment, equipasFragment).setTransition(
                FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()
        }

        linearLayoutCarros.setOnClickListener {
            val carrosFragment = CarrosFragment()

            val transaction = this.parentFragmentManager
            transaction.beginTransaction().replace(R.id.nav_host_fragment, carrosFragment).setTransition(
                FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()
        }

        linearLayoutPilotos.setOnClickListener {
            val pilotosFragment = PilotosFragment()

            val transaction = this.parentFragmentManager
            transaction.beginTransaction().replace(R.id.nav_host_fragment, pilotosFragment).setTransition(
                    FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()
        }

        return roowView
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.initail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_total -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.total.pt/"))
                startActivity(browserIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}