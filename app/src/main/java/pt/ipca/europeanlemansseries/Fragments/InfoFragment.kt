package pt.ipca.europeanlemansseries.Fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.LinearLayout
import androidx.fragment.app.FragmentTransaction
import pt.ipca.europeanlemansseries.R


class InfoFragment : Fragment() {

    // Widgets
    lateinit var rowView: View
    lateinit var linearLayoutApresentacao: LinearLayout
    lateinit var linearLayoutCategorias: LinearLayout
    lateinit var linearLayoutRegulamento: LinearLayout

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        rowView = inflater.inflate(R.layout.fragment_info, container, false)
        setHasOptionsMenu(true)

        linearLayoutApresentacao = rowView.findViewById(R.id.linearLayoutApresentacao)
        linearLayoutCategorias = rowView.findViewById(R.id.linearLayoutCategorias)
        linearLayoutRegulamento = rowView.findViewById(R.id.linearLayoutRegulamento)

        linearLayoutApresentacao.setOnClickListener {
            val apresentacaoFragment = ApresentacaoFragment()

            val transaction = this.parentFragmentManager
            transaction.beginTransaction().replace(R.id.nav_host_fragment, apresentacaoFragment).setTransition(
                    FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()
        }

        linearLayoutCategorias.setOnClickListener {
            val categoriasFragment = CategoriasFragment()

            val transaction = this.parentFragmentManager
            transaction.beginTransaction().replace(R.id.nav_host_fragment, categoriasFragment).setTransition(
                    FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()
        }

        linearLayoutRegulamento.setOnClickListener {
            val regulamentoFragment = RegulamentoFragment()

            val transaction = this.parentFragmentManager
            transaction.beginTransaction().replace(R.id.nav_host_fragment, regulamentoFragment).setTransition(
                    FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()
        }

        return rowView
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.initail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_total -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.total.pt/"))
                startActivity(browserIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}