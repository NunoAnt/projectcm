package pt.ipca.europeanlemansseries.Fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.*
import androidx.fragment.app.FragmentTransaction
import com.squareup.picasso.Picasso
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import pt.ipca.europeanlemansseries.Backend
import pt.ipca.europeanlemansseries.Models.Noticia
import pt.ipca.europeanlemansseries.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class NoticiasFragment : Fragment() {

    // Widgets
    lateinit var rootView: View
    lateinit var listViewNoticias: ListView

    var noticias: MutableList<Noticia> = ArrayList()
    lateinit var adapterNoticas: NoticiasAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_noticias, container, false)
        setHasOptionsMenu(true)

        listViewNoticias = rootView.findViewById(R.id.listViewNoticiasFragmentNoticias)

        adapterNoticas = NoticiasAdapter()
        listViewNoticias.adapter = adapterNoticas
        adapterNoticas.notifyDataSetChanged()

        // fazer pedido a API
        GlobalScope.async {
            noticias = Backend.getAllNotices(this@NoticiasFragment.requireContext())

            if (noticias.size == 0) {
                this@NoticiasFragment.activity?.runOnUiThread {
                    Toast.makeText(
                            this@NoticiasFragment.requireContext(),
                            "Não há notícias!",
                            Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                this@NoticiasFragment.activity?.runOnUiThread {
                    adapterNoticas.notifyDataSetChanged()
                }
            }
        }

        return rootView
    }

    inner class NoticiasAdapter : BaseAdapter() {
        override fun getCount(): Int {
            return noticias.size
        }

        override fun getItem(position: Int): Any {
            return noticias[position]
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val rowView = layoutInflater.inflate(R.layout.item_row_noticias, parent, false)

            val format = SimpleDateFormat("EEE, MMM dd", Locale.getDefault())

            val imageViewThumbnail = rowView.findViewById<ImageView>(R.id.imageViewItemRowNoticias)
            val textViewData = rowView.findViewById<TextView>(R.id.textViewItemRowNoticiasData)
            val textViewTitulo = rowView.findViewById<TextView>(R.id.textViewItemRowNiticiasTitulo)

            Picasso.get().load(noticias[position].thumbnail).fit().centerCrop().error(R.drawable.logo_elms).into(imageViewThumbnail)
            textViewData.text = format.format(noticias[position].data)
            textViewTitulo.text = noticias[position].titulo
            if (noticias[position].titulo.toString().length >= 30){
                textViewTitulo.text = noticias[position].titulo?.take(29)
                textViewTitulo.text = textViewTitulo.text as String + "..."
            } else
                textViewTitulo.text = noticias[position].titulo

            rowView.isClickable = true
            rowView.setOnClickListener {
                val noticiasDetailFragment = NoticiasDetailFragment()
                val args = Bundle()
                args.putInt(NoticiasDetailFragment.NOTICIA_ID, noticias[position].id!!)
                args.putString(NoticiasDetailFragment.NOTICIA_TITULO, noticias[position].titulo!!)
                args.putString(NoticiasDetailFragment.NOTICIA_THUMBNAIL, noticias[position].thumbnail!!)
                args.putString(NoticiasDetailFragment.NOTICIA_CONTEUDO, noticias[position].conteudo!!)
                args.putLong(NoticiasDetailFragment.NOTICIA_DATA, noticias[position].data?.time!!)
                noticiasDetailFragment.arguments = args

                val transaction = this@NoticiasFragment.parentFragmentManager
                transaction.beginTransaction().replace(R.id.nav_host_fragment, noticiasDetailFragment).setTransition(
                    FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()
            }

            return rowView
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.initail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_total -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.total.pt/"))
                startActivity(browserIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}