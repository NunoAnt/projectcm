package pt.ipca.europeanlemansseries.Fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.squareup.picasso.Picasso
import pt.ipca.europeanlemansseries.R


class NoticiasDetailFragment : Fragment() {

    companion object {
        const val NOTICIA_ID   = "noticia_id"
        const val NOTICIA_TITULO = "noticia_titulo"
        const val NOTICIA_THUMBNAIL = "noticia_thumbnail"
        const val NOTICIA_CONTEUDO = "noticia_conteudo"
        const val NOTICIA_DATA = "noticia_data  "
    }

    // Widgets
    lateinit var rootView: View
    lateinit var imageViewThumbnail: ImageView
    lateinit var textViewTitulo: TextView
    lateinit var textViewConteudo: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_noticias_detail, container, false)
        setHasOptionsMenu(true)

        imageViewThumbnail = rootView.findViewById(R.id.imageViewNoticiaDetailImagem)
        textViewTitulo = rootView.findViewById(R.id.textViewNoticialDetailTitulo)
        textViewConteudo = rootView.findViewById(R.id.textViewNoticialDetailConteudo)

        Picasso.get().load(arguments?.getString(NOTICIA_THUMBNAIL)).fit().centerCrop().error(R.drawable.logo_elms).into(imageViewThumbnail)
        textViewTitulo.text = arguments?.getString(NOTICIA_TITULO)
        textViewConteudo.text = arguments?.getString(NOTICIA_CONTEUDO)

        return rootView
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_notice, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_total -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.total.pt/"))
                startActivity(browserIntent)
                return true
            }
            R.id.action_share -> {
                val cutDescription: String = textViewConteudo.text.toString().substring(0, 173)

                val text = """
                    Notice: ${textViewTitulo.text.toString()}

                    Content: $cutDescription...
                    
                    Aplicação: https://www.europeanlemansseries.com/
                    """.trimIndent()

                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.putExtra(Intent.EXTRA_TEXT, text)
                shareIntent.type = "text/plain"
                startActivity(Intent.createChooser(shareIntent, "Partilhar"))

                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


}