package pt.ipca.europeanlemansseries.Fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.squareup.picasso.Picasso
import pt.ipca.europeanlemansseries.Models.Corrida
import pt.ipca.europeanlemansseries.R
import pt.ipca.europeanlemansseries.Utils.Utils
import java.util.*

class CorridaDetailFragment : Fragment() {

    companion object {
        const val CORRIDA_ID = "corrida_id"
        const val CORRIDA_NOME = "corrida_nome"
        const val CORRIDA_PAIS = "corrida_pais"
        const val CORRIDA_BANDEIRA_PAIS = "corrida_bandeira_pais"
        const val CORRIDA_DISTANCIA = "corrida_distancia"
        const val CORRIDA_CURVAS = "corrida_curvas"
        const val CORRIDA_MORADA = "corrida_morada"
        const val CORRIDA_TELEFONE = "corrida_telefone"
        const val CORRIDA_WEBSITE = "corrida_website"
        const val CORRIDA_FOTO = "corrida_foto"
        const val CORRIDA_TRACK_LAYOUT = "corrida_track_layout"
        const val CORRIDA_REPLAY = "corrida_replay"
        const val CORRIDA_DATA_INICIO = "corrida_data_inicio"
        const val CORRIDA_DATA_FIM = "corrida_data_fim"
    }

    // Widgets
    lateinit var rootView: View
    lateinit var linearLayoutReplay: LinearLayout
    lateinit var linearLayoutClassificacoes: LinearLayout
    lateinit var imageViewFotoCorrida: ImageView
    lateinit var textViewCircuit: TextView
    lateinit var textViewPais: TextView
    lateinit var textViewDistancia: TextView
    lateinit var textViewCurvas: TextView
    lateinit var textViewMorada: TextView
    lateinit var textViewTelefone: TextView
    lateinit var textViewWebsite: TextView
    lateinit var tabLayoutCorrida: TabLayout

    // Data Model
    lateinit var corrida: Corrida

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_corrida_detail, container, false)
        setHasOptionsMenu(true)

        // findViewById
        linearLayoutReplay = rootView.findViewById(R.id.linearLayoutDetalheCorridaReplay)
        linearLayoutClassificacoes = rootView.findViewById(R.id.linearLayoutDetalheCorridaClassificação)
        imageViewFotoCorrida = rootView.findViewById(R.id.imageViewDetalheCorridaFoto)
        textViewCircuit = rootView.findViewById(R.id.textViewDetalheCorridaCircuito)
        textViewPais = rootView.findViewById(R.id.textViewDetalheCorridaPais)
        textViewDistancia = rootView.findViewById(R.id.textViewDetalheCorridaDistancia)
        textViewCurvas = rootView.findViewById(R.id.textViewDetalheCorridaCurvas)
        textViewMorada = rootView.findViewById(R.id.textViewDetalheCorridaMorada)
        textViewTelefone = rootView.findViewById(R.id.textViewDetalheCorridaTelefone)
        textViewWebsite = rootView.findViewById(R.id.textViewDetalheCorridaWebsite)
        tabLayoutCorrida = rootView.findViewById(R.id.tabLayoutCategoria)

        // criar objeto
        val idCorrida = arguments?.getInt(CORRIDA_ID)
        val nomeCorrida = arguments?.getString(CORRIDA_NOME)
        val corridaPais = arguments?.getString(CORRIDA_PAIS)
        val corridaBandeiraPais = arguments?.getString(CORRIDA_BANDEIRA_PAIS)
        val corridaDistancia = arguments?.getDouble(CORRIDA_DISTANCIA)
        val corridaCurvas = arguments?.getInt(CORRIDA_CURVAS)
        val corridaMorada = arguments?.getString(CORRIDA_MORADA)
        val corridaTelefone = arguments?.getString(CORRIDA_TELEFONE)
        val corridaWebsite = arguments?.getString(CORRIDA_WEBSITE)
        val corridaFoto = arguments?.getString(CORRIDA_FOTO)
        val corridaTrackLayout = arguments?.getString(CORRIDA_TRACK_LAYOUT)
        val corridaReplay = arguments?.getString(CORRIDA_REPLAY)
        val corridaDataInicio = arguments?.getLong(CORRIDA_DATA_INICIO)
        val corridaDataFim = arguments?.getLong(CORRIDA_DATA_FIM)
        corrida = Corrida(
            idCorrida,
            nomeCorrida,
            corridaPais,
            corridaBandeiraPais,
            corridaDistancia,
            corridaCurvas,
            corridaMorada,
            corridaTelefone,
            corridaWebsite,
            corridaFoto,
            corridaTrackLayout,
            corridaReplay,
            corridaDataInicio?.let {
                Date(
                    it
                )
            },
            corridaDataFim?.let { Date(it) })

        // atualizar widgets
        Picasso.get().load(corrida.foto).fit().centerCrop().error(R.drawable.logo_elms).into(
            imageViewFotoCorrida
        )
        textViewCircuit.text = corrida.nome
        textViewPais.text = corrida.pais
        textViewDistancia.text = "".plus(corrida.distancia).plus("km").plus(" ").plus("/").plus(" ").plus(
            String.format(
                "%.02f",
                corrida.distancia?.let {
                    Utils.kmToMiles(
                        it
                    )
                })
        ).plus("miles")
        textViewCurvas.text = corrida.curvas.toString()
        textViewMorada.text = corrida.morada
        textViewTelefone.text = corrida.telefone
        textViewWebsite.text = corrida.website

        // abrir o replay da corrida
        linearLayoutReplay.setOnClickListener {
            val replayCorridaFragment = ReplayCorridaFragment()
            val args = Bundle()
            args.putString(ReplayCorridaFragment.REPLAY_ID, corrida.replay)
            args.putString(ReplayCorridaFragment.REPLAY_TITULO, corrida.nome)
            replayCorridaFragment.arguments = args

            val transaction = this@CorridaDetailFragment.parentFragmentManager
            transaction.beginTransaction().replace(R.id.nav_host_fragment, replayCorridaFragment).setTransition(
                FragmentTransaction.TRANSIT_FRAGMENT_FADE
            ).commit()
        }

        /* abrir o 1º fragment */
        val fragmentOverview = OverviewFragment()
        val args = Bundle()
        corrida.id?.let { it1 ->
            args.putInt(
                OverviewFragment.CORRIDA_ID,
                it1
            )
        }
        args.putString(OverviewFragment.CORRIDA_NOME, corrida.nome)
        args.putString(OverviewFragment.CORRIDA_PAIS, corrida.pais)
        args.putString(OverviewFragment.CORRIDA_BANDEIRA_PAIS, corrida.bandeiraPais)
        corrida.distancia?.let { it1 ->
            args.putDouble(
                OverviewFragment.CORRIDA_DISTANCIA,
                it1
            )
        }
        corrida.curvas?.let { it1 ->
            args.putInt(
                OverviewFragment.CORRIDA_CURVAS,
                it1
            )
        }
        args.putString(OverviewFragment.CORRIDA_MORADA, corrida.morada)
        args.putString(OverviewFragment.CORRIDA_TELEFONE, corrida.telefone)
        args.putString(OverviewFragment.CORRIDA_WEBSITE, corrida.website)
        args.putString(OverviewFragment.CORRIDA_FOTO, corrida.foto)
        args.putString(OverviewFragment.CORRIDA_TRACK_LAYOUT, corrida.trackLayout)
        args.putString(OverviewFragment.CORRIDA_REPLAY, corrida.replay)
        corrida.dataInicio?.time?.let { it1 ->
            args.putLong(
                OverviewFragment.CORRIDA_DATA_INICIO,
                it1
            )
        }
        corrida.dataFim?.time?.let { it1 ->
            args.putLong(
                OverviewFragment.CORRIDA_DATA_FIM,
                it1
            )
        }
        fragmentOverview.arguments = args

        // abrir o 1º fragment
        val transaction = this@CorridaDetailFragment.parentFragmentManager
        transaction.beginTransaction().replace(R.id.containerCorridaDetail, fragmentOverview).setTransition(
            FragmentTransaction.TRANSIT_FRAGMENT_FADE
        ).commit() /* abrir o 1º fragment */

        /* TabView */
        tabLayoutCorrida.setTabGravity(TabLayout.GRAVITY_FILL);

        tabLayoutCorrida.addTab(tabLayoutCorrida.newTab().setText("OVERVIEW"))
        tabLayoutCorrida.addTab(tabLayoutCorrida.newTab().setText("TRACK"))


        tabLayoutCorrida.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                OpenFragment(tab.position)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        }) /* TabView */

        // abrir as classificacoes
        linearLayoutClassificacoes.setOnClickListener {
            val resultadoFragment = ResultadoFragment()
            val args = Bundle()
            fragmentOverview.getRaceSesson()?.let { it1 -> args.putInt(ResultadoFragment.SESSAO_ID, it1) }
            resultadoFragment.arguments = args

            // abrir o 1º fragment
            val transaction = this@CorridaDetailFragment.parentFragmentManager
            transaction.beginTransaction().replace(R.id.nav_host_fragment, resultadoFragment).setTransition(
                    FragmentTransaction.TRANSIT_FRAGMENT_FADE
            ).commit()
        }

        return rootView
    }

    // Abrir o Fragmento conforme a TAB
    fun OpenFragment(position: Int) {
        if (position == 0) {
            /* abrir o 1º fragment */
            val fragmentOverview = OverviewFragment()
            val args = Bundle()
            corrida.id?.let { it1 ->
                args.putInt(
                    OverviewFragment.CORRIDA_ID,
                    it1
                )
            }
            args.putString(OverviewFragment.CORRIDA_NOME, corrida.nome)
            args.putString(OverviewFragment.CORRIDA_PAIS, corrida.pais)
            args.putString(OverviewFragment.CORRIDA_BANDEIRA_PAIS, corrida.bandeiraPais)
            corrida.distancia?.let { it1 ->
                args.putDouble(
                    OverviewFragment.CORRIDA_DISTANCIA,
                    it1
                )
            }
            corrida.curvas?.let { it1 ->
                args.putInt(
                    OverviewFragment.CORRIDA_CURVAS,
                    it1
                )
            }
            args.putString(OverviewFragment.CORRIDA_MORADA, corrida.morada)
            args.putString(OverviewFragment.CORRIDA_TELEFONE, corrida.telefone)
            args.putString(OverviewFragment.CORRIDA_WEBSITE, corrida.website)
            args.putString(OverviewFragment.CORRIDA_FOTO, corrida.foto)
            args.putString(OverviewFragment.CORRIDA_TRACK_LAYOUT, corrida.trackLayout)
            args.putString(OverviewFragment.CORRIDA_REPLAY, corrida.replay)
            corrida.dataInicio?.time?.let { it1 ->
                args.putLong(
                    OverviewFragment.CORRIDA_DATA_INICIO,
                    it1
                )
            }
            corrida.dataFim?.time?.let { it1 ->
                args.putLong(
                    OverviewFragment.CORRIDA_DATA_FIM,
                    it1
                )
            }
            fragmentOverview.arguments = args

            // abrir o 1º fragment
            val transaction = this@CorridaDetailFragment.parentFragmentManager
            transaction.beginTransaction().replace(R.id.containerCorridaDetail, fragmentOverview).setTransition(
                FragmentTransaction.TRANSIT_FRAGMENT_FADE
            ).commit() /* abrir o 1º fragment */
        } else if (position == 1) {
            /* abrir o 1º fragment */
            val fragmentTrack = TrackLayoutFragment()
            val args = Bundle()
            args.putString(TrackLayoutFragment.TRACK_LAYOUT, corrida.trackLayout)
            fragmentTrack.arguments = args

            // abrir o 1º fragment
            val transaction = this@CorridaDetailFragment.parentFragmentManager
            transaction.beginTransaction().replace(R.id.containerCorridaDetail, fragmentTrack).setTransition(
                FragmentTransaction.TRANSIT_FRAGMENT_FADE
            ).commit() /* abrir o 1º fragment */
        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.initail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_total -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.total.pt/"))
                startActivity(browserIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}