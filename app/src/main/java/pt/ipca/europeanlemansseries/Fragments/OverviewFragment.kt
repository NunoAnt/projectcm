package pt.ipca.europeanlemansseries.Fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.squareup.picasso.Picasso
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import pt.ipca.europeanlemansseries.Backend
import pt.ipca.europeanlemansseries.Models.Corrida
import pt.ipca.europeanlemansseries.Models.Sessao
import pt.ipca.europeanlemansseries.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class OverviewFragment : Fragment() {

    companion object {
        const val CORRIDA_ID = "corrida_id"
        const val CORRIDA_NOME = "corrida_nome"
        const val CORRIDA_PAIS = "corrida_pais"
        const val CORRIDA_BANDEIRA_PAIS = "corrida_bandeira_pais"
        const val CORRIDA_DISTANCIA = "corrida_distancia"
        const val CORRIDA_CURVAS = "corrida_curvas"
        const val CORRIDA_MORADA = "corrida_morada"
        const val CORRIDA_TELEFONE = "corrida_telefone"
        const val CORRIDA_WEBSITE = "corrida_website"
        const val CORRIDA_FOTO = "corrida_foto"
        const val CORRIDA_TRACK_LAYOUT = "corrida_track_layout"
        const val CORRIDA_REPLAY = "corrida_replay"
        const val CORRIDA_DATA_INICIO = "corrida_data_inicio"
        const val CORRIDA_DATA_FIM = "corrida_data_fim"
    }

    // Widgets
    lateinit var rootView: View
    lateinit var imageViewBandeiraPais: ImageView
    lateinit var textViewPais: TextView
    lateinit var textViewNomeCorrida: TextView
    lateinit var listViewSessoes: ListView

    // Data Model
    lateinit var corrida: Corrida
    var sessoes: MutableList<Sessao> = ArrayList()
    lateinit var adapterSessoes: SessoesAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_overview, container, false)

        // findViewById
        imageViewBandeiraPais = rootView.findViewById(R.id.imageViewOvewviewBandeiraPais)
        textViewPais = rootView.findViewById(R.id.textViewOverviewPais)
        textViewNomeCorrida = rootView.findViewById(R.id.textViewOverviewNomeCorrida)
        listViewSessoes = rootView.findViewById(R.id.listViewOverviewSessoes)

        // criar objeto
        val idCorrida = arguments?.getInt(CORRIDA_ID)
        val nomeCorrida = arguments?.getString(CORRIDA_NOME)
        val corridaPais = arguments?.getString(CORRIDA_PAIS)
        val corridaBandeiraPais = arguments?.getString(CORRIDA_BANDEIRA_PAIS)
        val corridaDistancia = arguments?.getDouble(CORRIDA_DISTANCIA)
        val corridaCurvas = arguments?.getInt(CORRIDA_CURVAS)
        val corridaMorada = arguments?.getString(CORRIDA_MORADA)
        val corridaTelefone = arguments?.getString(CORRIDA_TELEFONE)
        val corridaWebsite = arguments?.getString(CORRIDA_WEBSITE)
        val corridaFoto = arguments?.getString(CORRIDA_FOTO)
        val corridaTrackLayout = arguments?.getString(CORRIDA_TRACK_LAYOUT)
        val corridaReplay = arguments?.getString(CORRIDA_REPLAY)
        val corridaDataInicio = arguments?.getLong(CORRIDA_DATA_INICIO)
        val corridaDataFim = arguments?.getLong(CORRIDA_DATA_FIM)
        corrida = Corrida(idCorrida, nomeCorrida, corridaPais, corridaBandeiraPais, corridaDistancia, corridaCurvas, corridaMorada, corridaTelefone, corridaWebsite, corridaFoto, corridaTrackLayout, corridaReplay, corridaDataInicio?.let { Date(it) }, corridaDataFim?.let { Date(it) })

        // atualizar widgets
        Picasso.get().load(corrida.bandeiraPais).fit().centerInside().error(R.drawable.logo_elms).into(imageViewBandeiraPais)
        textViewPais.text = corrida.pais
        textViewNomeCorrida.text = corrida.nome


        adapterSessoes = SessoesAdapter()
        listViewSessoes.adapter = adapterSessoes
        adapterSessoes.notifyDataSetChanged()

        GlobalScope.async {
            sessoes = Backend.getSessonsRaces(this@OverviewFragment.requireContext(), corrida.id!!)

            if (sessoes.size == 0) {
                this@OverviewFragment.activity?.runOnUiThread {
                    Toast.makeText(
                            this@OverviewFragment.requireContext(),
                            "Não há sessoes!",
                            Toast.LENGTH_LONG
                    ).show()

                }
            } else {
                this@OverviewFragment.activity?.runOnUiThread {
                    adapterSessoes.notifyDataSetChanged()
                }
            }
        }

        return rootView
    }

    fun getRaceSesson(): Int? {
        return sessoes.last().id
    }

    inner class SessoesAdapter: BaseAdapter() {
        override fun getCount(): Int {
            return sessoes.size
        }

        override fun getItem(position: Int): Any {
            return sessoes[position]
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val rowView = layoutInflater.inflate(R.layout.item_row_sessoes, parent, false)
            val textViewNome = rowView.findViewById<TextView>(R.id.textViewItemRowSessaoNome)
            val textViewDia = rowView.findViewById<TextView>(R.id.textViewItemRowSessaoDia)
            val textViewHoras = rowView.findViewById<TextView>(R.id.textViewItemRowSessaoHoras)

            val format = SimpleDateFormat("EEE", Locale.getDefault())
            val hora = SimpleDateFormat("HH:mm", Locale.getDefault())
            textViewNome.text = sessoes[position].nome
            textViewDia.text = format.format(sessoes[position].dia).toString().toUpperCase()
            textViewHoras.text = "".plus(hora.format(sessoes[position].horaInicio)).plus("-").plus(hora.format(sessoes[position].horaFim))

            return rowView
        }
    }
}