package pt.ipca.europeanlemansseries.Fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import com.squareup.picasso.Picasso
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import pt.ipca.europeanlemansseries.Backend
import pt.ipca.europeanlemansseries.Models.Enums.Categoria
import pt.ipca.europeanlemansseries.Models.Resultado
import pt.ipca.europeanlemansseries.R

class ResultadoFragment : Fragment() {

    companion object {
        const val SESSAO_ID = "sessao_id"
    }

    lateinit var rootView: View
    lateinit var listViewResultados: ListView

    var resultados: MutableList<Resultado> = ArrayList()
    lateinit var adapterResultado: ResultadosAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_resultado, container, false)
        setHasOptionsMenu(true)

        listViewResultados = rootView.findViewById(R.id.listViewClassificacao)

        // obter o ID da sessao
        val idSessao = arguments?.getInt(SESSAO_ID)

        adapterResultado = ResultadosAdapter()
        listViewResultados.adapter = adapterResultado
        adapterResultado.notifyDataSetChanged()

        // fazer pedido a API
        GlobalScope.async {
            resultados = Backend.getRaceResult(this@ResultadoFragment.requireContext(), idSessao!!)

            if (resultados.size == 0) {
                this@ResultadoFragment.activity?.runOnUiThread {
                    Toast.makeText(
                            this@ResultadoFragment.requireContext(),
                            "Não há resultado!",
                            Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                this@ResultadoFragment.activity?.runOnUiThread {
                    adapterResultado.notifyDataSetChanged()
                }
            }
        }

        return rootView
    }

    inner class ResultadosAdapter: BaseAdapter() {
        override fun getCount(): Int {
            return resultados.size
        }

        override fun getItem(position: Int): Any {
            return resultados[position]
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val rowView = layoutInflater.inflate(R.layout.item_row_resultado, parent, false)

            val textViewPosicao = rowView.findViewById<TextView>(R.id.textViewItemRowResultadoPosicao)
            val textViewNumeroCarro = rowView.findViewById<TextView>(R.id.textViewItemRowResultadoNumeroCarro)
            val textViewNomeEquipa = rowView.findViewById<TextView>(R.id.textViewItemRowResultadoNomeEquipa)
            val textViewNomePiloto1 = rowView.findViewById<TextView>(R.id.textViewItemRowResultadoNomePiloto1)
            val textViewNomePiloto2 = rowView.findViewById<TextView>(R.id.textViewItemRowResultadoNomePiloto2)
            val textViewNomePiloto3 = rowView.findViewById<TextView>(R.id.textViewItemRowResultadoNomePiloto3)
            val imageViewCarro = rowView.findViewById<ImageView>(R.id.imageViewItemRowResultadoImagemCarro)


            textViewPosicao.text = resultados[position].classificacao.toString()
            textViewNumeroCarro.text = resultados[position].carro?.numero.toString()
            textViewNomeEquipa.text = resultados[position].equipa?.nome.toString()
            textViewNomePiloto1.text = resultados[position].pilotos!![0].nome
            textViewNomePiloto2.text = resultados[position].pilotos!![1].nome
            // verificar numero de pilotos para definir o numero de textsViews visiveis
            if (resultados[position].pilotos?.size!! < 3) {
                textViewNomePiloto3.visibility = View.GONE
            } else {
                textViewNomePiloto3.text = resultados[position].pilotos!![2].nome
            }
            Picasso.get().load(resultados[position].carro?.imagemCarro).fit().centerInside().error(R.drawable.logo_elms).into(imageViewCarro)

            // mudar a cor do numero conforme a categoria
            when(resultados[position].categoria) {
                Categoria.GTELM -> {
                    val gtelm = this@ResultadoFragment.requireContext().getResources().getColor(R.color.GTELM)
                    textViewNumeroCarro.setBackgroundColor(gtelm)
                }
                Categoria.LMP3 -> {
                    val lmp3 = this@ResultadoFragment.requireContext().getResources().getColor(R.color.LMP3)
                    textViewNumeroCarro.setBackgroundColor(lmp3)
                }
                Categoria.LMP2 -> {
                    val lmp2 = this@ResultadoFragment.requireContext().getResources().getColor(R.color.LMP2)
                    textViewNumeroCarro.setBackgroundColor(lmp2)
                }
            }

            return rowView
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.initail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_total -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.total.pt/"))
                startActivity(browserIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}