package pt.ipca.europeanlemansseries.Fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import okhttp3.internal.http.HttpMethod
import pt.ipca.europeanlemansseries.Backend
import pt.ipca.europeanlemansseries.Models.Carro
import pt.ipca.europeanlemansseries.R


class CarrosFragment : Fragment() {

    // Widgets
    lateinit var rootView: View
    lateinit var listViewCarros: ListView
    lateinit var editTextSearchCarro: EditText

    // Lista carros
    var carros: MutableList<Carro> = ArrayList()
    var carrosSearch: MutableList<Carro> = ArrayList()
    lateinit var adapterCarros: CarrosAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_carros, container, false)
        setHasOptionsMenu(true)

        // findViewById
        listViewCarros = rootView.findViewById(R.id.listViewCarros)
        editTextSearchCarro = rootView.findViewById(R.id.editTextCarrosSearch)

        adapterCarros = CarrosAdapter()
        listViewCarros.adapter = adapterCarros
        adapterCarros.notifyDataSetChanged()

        // fazer pedido a API
        GlobalScope.async {
            carros = Backend.getCars(this@CarrosFragment.requireContext())

            if (carros.size == 0) {
                this@CarrosFragment.activity?.runOnUiThread {
                    Toast.makeText(
                        this@CarrosFragment.requireContext(),
                        "Não há carros!",
                        Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                this@CarrosFragment.activity?.runOnUiThread {
                    carrosSearch = carros
                    adapterCarros.notifyDataSetChanged()
                }
            }
        }

        // Procurar na Lista
        editTextSearchCarro.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                filter(editTextSearchCarro.text.toString())
            }

        })

        return rootView
    }

    private fun filter(text: String) {
        carrosSearch = carros.filter { carro ->  "#".plus(carro.numero).plus(" ").plus(carro.modelo).plus(
            " "
        ).plus("-").plus(" ").plus(carro.fabricante).toLowerCase()?.contains(text.toLowerCase()) == true
        } as MutableList<Carro>
        adapterCarros.notifyDataSetChanged()
    }

    inner class CarrosAdapter: BaseAdapter() {
        override fun getCount(): Int {
            return carrosSearch.size
        }

        override fun getItem(position: Int): Any {
            return carrosSearch[position]
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val rowView = layoutInflater.inflate(R.layout.item_row_carros, parent, false)

            val textViewNome = rowView.findViewById<TextView>(R.id.textViewItemRowCarrosNome)

            textViewNome.text = "#".plus(carrosSearch[position].numero).plus(" ").plus(carrosSearch[position].modelo).plus(
                " "
            ).plus("-").plus(" ").plus(carrosSearch[position].fabricante)

            rowView.isClickable = true
            rowView.setOnClickListener {
                var carroDetailFragment = CarroDetailFragment()
                val args = Bundle()
                carrosSearch[position].id?.let { it1 ->
                    args.putInt(
                        CarroDetailFragment.CARRO_ID,
                        it1
                    )
                }
                carrosSearch[position].equipa?.id?.let { it1 ->
                    args.putInt(
                        CarroDetailFragment.CARRO_ID_EQUIPA,
                        it1
                    )
                }
                args.putString(CarroDetailFragment.CARRO_NOME, carrosSearch[position].nome)
                carrosSearch[position].numero?.let { it1 ->
                    args.putInt(
                        CarroDetailFragment.CARRO_NUMERO,
                        it1
                    )
                }
                args.putString(CarroDetailFragment.CARRO_MODELO, carrosSearch[position].modelo)
                args.putString(
                    CarroDetailFragment.CARRO_FABRICANTE,
                    carrosSearch[position].fabricante
                )
                args.putString(
                    CarroDetailFragment.CARRO_MARCA_PNEUS,
                    carrosSearch[position].marcaPneus
                )
                args.putString(
                    CarroDetailFragment.CARRO_IMAGEM_CARRO,
                    carrosSearch[position].imagemCarro
                )
                carroDetailFragment.arguments = args


                val transaction = this@CarrosFragment.parentFragmentManager
                transaction.beginTransaction().replace(R.id.nav_host_fragment, carroDetailFragment).setTransition(
                    FragmentTransaction.TRANSIT_FRAGMENT_FADE
                ).commit()
            }


            return rowView
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.initail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_total -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.total.pt/"))
                startActivity(browserIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
