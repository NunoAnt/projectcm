package pt.ipca.europeanlemansseries.Fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import pt.ipca.europeanlemansseries.Adapters.NoticiasAdapter
import pt.ipca.europeanlemansseries.Backend
import pt.ipca.europeanlemansseries.Models.Noticia
import pt.ipca.europeanlemansseries.Models.Piloto
import pt.ipca.europeanlemansseries.R
import java.text.SimpleDateFormat
import java.util.*

class PilotoDetailFragment : Fragment() {

    companion object {
        const val PILOTO_ID   = "piloto_id"
        const val PILOTO_CARRO_ID = "piloto_carro_id"
        const val PILOTO_NOME = "piloto_nome"
        const val PILOTO_IDADE = "piloto_idade"
        const val PILOTO_DATA_NASCIMENTO = "piloto_data_nascimento"
        const val PILOTO_PAIS = "piloto_pais"
        const val PILOTO_BANDEIRA_PAIS = "piloto_bandeira_pais"
        const val PILOTO_FACEBOOK = "piloto_facebook"
        const val PILOTO_TWITTER = "piloto_twitter"
        const val PILOTO_WEBSITE = "piloto_website"
        const val PILOTO_FOTO = "piloto_foto"
    }


    // Widgets
    lateinit var rootView: View
    lateinit var imageViewPiloto: ImageView
    lateinit var imageViewBandeiraPais: ImageView
    lateinit var textViewPais: TextView
    lateinit var textViewNomePiloto: TextView
    lateinit var imageViewFacebook: ImageView
    lateinit var imageViewTwitter: ImageView
    lateinit var imageViewWebsite: ImageView
    lateinit var textViewIdade: TextView
    lateinit var textViewDataNascimento: TextView
    lateinit var textViewNacionalidade: TextView
    lateinit var imageViewCarro: ImageView
    lateinit var textViewNomeCarro: TextView
    lateinit var recyclerViewNoticias: RecyclerView

    // ModelDate
    lateinit var piloto: Piloto

    // lista noticias
    var noticias: MutableList<Noticia> = ArrayList()
    lateinit var noticiasAdapter: NoticiasAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_piloto_detail, container, false)
        setHasOptionsMenu(true)

        // inicializar os objetos
        imageViewPiloto = rootView.findViewById(R.id.imageViewDetalhePilotoImagem)
        imageViewBandeiraPais = rootView.findViewById(R.id.imageViewDetalhePilotoBandeiraImagem)
        textViewPais = rootView.findViewById(R.id.textViewDetalhePilotoNomePais)
        textViewNomePiloto = rootView.findViewById(R.id.textViewDetalhePilotoNomePiloto)
        imageViewFacebook = rootView.findViewById(R.id.imageViewdetalhePilotoFacebook)
        imageViewTwitter = rootView.findViewById(R.id.imageViewDetalhePilotoTwitter)
        imageViewWebsite = rootView.findViewById(R.id.imageViewDetalhePilotoWebsite)
        textViewIdade = rootView.findViewById(R.id.textViewDetalhePilotoIdade)
        textViewDataNascimento = rootView.findViewById(R.id.textViewDetalhePilotoDataNascimento)
        textViewNacionalidade = rootView.findViewById(R.id.textViewDetalhePilotoNacionalidade)
        imageViewCarro = rootView.findViewById(R.id.imageViewDetalhePilotoImagemCarro)
        textViewNomeCarro = rootView.findViewById(R.id.textViewDetalhePilotoNomeCarro)
        recyclerViewNoticias = rootView.findViewById(R.id.recyclerViewDetalhePilotoNotica)

        // Criar objeto
        val idPiloto = arguments?.getInt(PILOTO_ID)
        val idCarro = arguments?.getInt(PILOTO_CARRO_ID)
        val nome = arguments?.getString(PILOTO_NOME)
        val idade = arguments?.getInt(PILOTO_IDADE)
        val dataNascimento = arguments?.getLong(PILOTO_DATA_NASCIMENTO)
        val pais = arguments?.getString(PILOTO_PAIS)
        val bandeiraPais = arguments?.getString(PILOTO_BANDEIRA_PAIS)
        val facebook = arguments?.getString(PILOTO_FACEBOOK)
        val twitter = arguments?.getString(PILOTO_TWITTER)
        val webSite = arguments?.getString(PILOTO_WEBSITE)
        val foto = arguments?.getString(PILOTO_FOTO)
        piloto = Piloto(idPiloto, nome, idade, dataNascimento?.let { Date(it) }, pais, bandeiraPais, facebook, twitter, webSite, foto)

        // atualizar widgets
        Picasso.get().load(piloto.foto).fit().centerInside().error(R.drawable.logo_elms).into(imageViewPiloto)
        Picasso.get().load(piloto.bandeiraPais).fit().centerInside().error(R.drawable.logo_elms).into(imageViewBandeiraPais)
        textViewPais.text = piloto.pais
        textViewNomePiloto.text = piloto.nome

        // abrir link do facebook
        imageViewFacebook.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(piloto.facebook))
            startActivity(browserIntent)
        }

        // abrir link do twitter
        imageViewTwitter.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(piloto.twitter))
            startActivity(browserIntent)
        }

        // abrir link do website
        imageViewWebsite.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(piloto.webSite))
            startActivity(browserIntent)
        }

        // atualizar os restantes widgets
        val format = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        textViewIdade.text = piloto.idade.toString()
        textViewDataNascimento.text = format.format(piloto.dataNascimento)
        textViewNacionalidade.text = piloto.pais

        GlobalScope.async {
            piloto.carro = idCarro?.let { Backend.getCar(this@PilotoDetailFragment.requireContext(), it) }

            this@PilotoDetailFragment.activity?.runOnUiThread {
                // atualizar widgets carro
                Picasso.get().load(piloto.carro?.imagemCarro).fit().centerInside().error(R.drawable.logo_elms).into(imageViewCarro)
                textViewNomeCarro.text = "#".plus(piloto.carro?.numero.toString()).plus(" ").plus(piloto.carro?.modelo)
            }
        }

        noticiasAdapter = NoticiasAdapter(noticias, requireActivity().applicationContext, this.parentFragmentManager)

        val layoutManager = LinearLayoutManager(
                this@PilotoDetailFragment.requireContext(),
                LinearLayoutManager.VERTICAL,
                false
        )
        recyclerViewNoticias.layoutManager = layoutManager
        noticiasAdapter.notifyDataSetChanged()
        recyclerViewNoticias.adapter = noticiasAdapter

        // fazer pedido a API
        GlobalScope.async {
            noticias = Backend.getLastTreeNotices(this@PilotoDetailFragment.requireContext())

            if (noticias.size == 0) {
                this@PilotoDetailFragment.activity?.runOnUiThread {
                    Toast.makeText(
                            this@PilotoDetailFragment.requireContext(),
                            "Não há Notícias!",
                            Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                this@PilotoDetailFragment.activity?.runOnUiThread {
                    noticiasAdapter = NoticiasAdapter(noticias, requireActivity().applicationContext, this@PilotoDetailFragment.parentFragmentManager)

                    val layoutManager = LinearLayoutManager(
                            this@PilotoDetailFragment.requireContext(),
                            LinearLayoutManager.VERTICAL,
                            false
                    )
                    recyclerViewNoticias.layoutManager = layoutManager
                    noticiasAdapter.notifyDataSetChanged()
                    recyclerViewNoticias.adapter = noticiasAdapter

                    noticiasAdapter.notifyDataSetChanged()
                }
            }
        }

        return rootView
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.initail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_total -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.total.pt/"))
                startActivity(browserIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}