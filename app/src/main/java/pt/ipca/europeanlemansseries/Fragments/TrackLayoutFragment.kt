package pt.ipca.europeanlemansseries.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso
import pt.ipca.europeanlemansseries.R

class TrackLayoutFragment : Fragment() {

    companion object {
        const val TRACK_LAYOUT = "corrida_track_layout"
    }


    // Widgets
    lateinit var rootView: View
    lateinit var imageViewTrackLayout: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_track_layout, container, false)

        // findViewById
        imageViewTrackLayout = rootView.findViewById(R.id.imageViewTrackLayout)

        // Abrir imagem
        Picasso.get().load(arguments?.getString(TRACK_LAYOUT)).fit().centerInside().error(R.drawable.logo_elms).into(imageViewTrackLayout)

        return rootView
    }
}