package pt.ipca.europeanlemansseries.Fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.TextView
import pt.ipca.europeanlemansseries.R

class RegulamentoFragment : Fragment() {

    lateinit var rootView: View
    lateinit var textViewAboutRegulamentoPneus: TextView
    lateinit var textViewRegulamentPneusLMP3: TextView
    lateinit var textViewRegulamentoLMGTE: TextView
    lateinit var textViewRegulamentoClassification: TextView
    lateinit var textViewUrlDownload: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_regulamento, container, false)
        setHasOptionsMenu(true)

        textViewAboutRegulamentoPneus = rootView.findViewById(R.id.textViewAboutRegulationTyers)
        textViewRegulamentPneusLMP3 = rootView.findViewById(R.id.textViewAboutRegulationTyersLmp3)
        textViewRegulamentoLMGTE = rootView.findViewById(R.id.textViewAboutRegulationLMGTE)
        textViewRegulamentoClassification = rootView.findViewById(R.id.textViewAboutRegulationClassification)
        textViewUrlDownload = rootView.findViewById(R.id.textViewDownloadRegulamentation)

        textViewAboutRegulamentoPneus.text = "As previously announced, from 2021, Goodyear will become the sole tyre manufacturer for the LMP2 class, in addition to the LMGTE category.\n\n" +
                "In LMP3, Michelin stays the dedicated tyre manufacturer."

        textViewRegulamentPneusLMP3.text = "An additional set of tyres have been allocated to the LMP3 cars. The teams will now be able to use 4 sets of tyres instead of 3 the previous seasons."

        textViewRegulamentoLMGTE.text = "The driving time for the LMGTE crews comprised as followed, Bronze/ Silver/ Silver have been updated. The Bronze drivers pairing with two Silver drivers will now have a minimum driving time of 1h30 instead of 45 minutes the previous season. This means that now, no matter the line-up, all the Bronze drivers in LMGTE will have a minimum driving time of 1h30."

        textViewRegulamentoClassification.text = "No amendments have been made concerning the LMP2 line-ups and the basis of the rules remain the same as in 2020.\n\n" +
                "A new classification has been created, reserved for the Pro/ Am line-ups in LMP2 (i.e every line up with a Bronze driver).\n\n" +
                "A dedicated LMP2 Pro/ Am podium will be organised at the end of each race.\n\n" +
                "At the end of the season a trophy will be awarded to the first Competitor of the LMP2 Pro/ Am classification (team and drivers)."

        textViewUrlDownload.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://storage.googleapis.com/elms-prod/assets/PDF/2021/2021_European%20Le%20Mans%20Series_Sporting%20Regulation_V1_clean_version_03122020.pdf"))
            startActivity(browserIntent)
        }

        return rootView
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.initail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_total -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.total.pt/"))
                startActivity(browserIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}