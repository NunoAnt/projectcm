package pt.ipca.europeanlemansseries.Fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import pt.ipca.europeanlemansseries.Backend
import pt.ipca.europeanlemansseries.Models.Enums.Categoria
import pt.ipca.europeanlemansseries.Models.Equipa
import pt.ipca.europeanlemansseries.Models.Piloto
import pt.ipca.europeanlemansseries.R

class EquipasFragment : Fragment() {

    // Widgets
    private lateinit var rootView: View
    lateinit var listViewEquipas: ListView
    lateinit var editTextEquipaSearch: EditText

    var equipas: MutableList<Equipa> = ArrayList()
    var equipasSearch: MutableList<Equipa> = ArrayList()
    lateinit var adapterEquipas: EquipasAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_equipas, container, false)
        setHasOptionsMenu(true)

        listViewEquipas = rootView.findViewById(R.id.listViewEquipas)
        editTextEquipaSearch = rootView.findViewById(R.id.editTextEquipasSearch)

        adapterEquipas = EquipasAdapter()
        listViewEquipas.adapter = adapterEquipas
        adapterEquipas.notifyDataSetChanged()


        GlobalScope.async {
            equipas = Backend.getAllTeams(this@EquipasFragment.requireContext())

            if (equipas.size == 0) {
                this@EquipasFragment.activity?.runOnUiThread {
                    Toast.makeText(
                        this@EquipasFragment.requireContext(),
                        "Não há equipas!",
                        Toast.LENGTH_LONG
                    ).show()

                }
            } else {
                this@EquipasFragment.activity?.runOnUiThread {
                    equipasSearch = equipas
                    adapterEquipas.notifyDataSetChanged()
                }
            }
        }

        // Procurar na Lista
        editTextEquipaSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                filter(editTextEquipaSearch.text.toString())
            }

        })

        return rootView
    }

    private fun filter(text: String) {
        equipasSearch = equipas.filter {
                equipa ->  equipa.nome?.toLowerCase()?.contains(text.toLowerCase()) == true
        } as MutableList<Equipa>
        adapterEquipas.notifyDataSetChanged()
    }


    inner class EquipasAdapter : BaseAdapter() {
        override fun getCount(): Int {
            return equipasSearch.size
        }

        override fun getItem(position: Int): Any {
            return equipasSearch[position]
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val rowView = layoutInflater.inflate(R.layout.item_row_equipas, parent, false)
            val textViewNomeEquipa = rowView.findViewById<TextView>(R.id.textViewItemRowEquipasNome)

            textViewNomeEquipa.text = equipasSearch[position].nome

            rowView.isClickable = true
            rowView.setOnClickListener {
                var equipaDetailFragment = EquipaDetailFragment()
                val args = Bundle()
                equipasSearch[position].id?.let { it1 ->
                    args.putInt(EquipaDetailFragment.EQUIPA_ID,
                        it1
                    )
                }
                args.putString(EquipaDetailFragment.EQUIPA_NOME, equipasSearch[position].nome)
                equipasSearch[position].categoria?.let { it1 -> Equipa.setCategoria(it1) }?.let { it2 ->
                    args.putInt(EquipaDetailFragment.EQUIPA_CATEGORIA,
                        it2
                    )
                }
                args.putString(EquipaDetailFragment.EQUIPA_BANDEIRA_PAIS, equipasSearch[position].bandeiraPais)
                args.putString(EquipaDetailFragment.EQUIPA_LOGO, equipasSearch[position].logo)
                args.putString(EquipaDetailFragment.EQUIPA_FACEBOOK, equipasSearch[position].facebook)
                args.putString(EquipaDetailFragment.EQUIPA_TWITTER, equipasSearch[position].twitter)
                args.putString(EquipaDetailFragment.EQUIPA_WEBSITE, equipasSearch[position].website)
                equipaDetailFragment.arguments = args

                val transaction = this@EquipasFragment.parentFragmentManager
                transaction.beginTransaction().replace(R.id.nav_host_fragment, equipaDetailFragment).setTransition(
                        FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()
            }

            return rowView
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.initail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_total -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.total.pt/"))
                startActivity(browserIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}