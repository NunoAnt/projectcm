package pt.ipca.europeanlemansseries.Fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import com.squareup.picasso.Picasso
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import pt.ipca.europeanlemansseries.Backend
import pt.ipca.europeanlemansseries.Models.Classificacao
import pt.ipca.europeanlemansseries.Models.Enums.Categoria
import pt.ipca.europeanlemansseries.Models.Noticia
import pt.ipca.europeanlemansseries.R

class ClassificacaoFragment : Fragment() {

    lateinit var rootView: View
    lateinit var listViewClassificacao: ListView
    lateinit var tabLayoutClassificacao: TabLayout

    var classificacoes: MutableList<Classificacao> = ArrayList()
    var listaClassificacoes: MutableList<Classificacao> = ArrayList()
    lateinit var classificacaoAdapter: ClassificacoesAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_classificacao, container, false)
        setHasOptionsMenu(true)

        listViewClassificacao = rootView.findViewById(R.id.listViewClassificacao)
        tabLayoutClassificacao = rootView.findViewById(R.id.tabLayoutCategoria)

        classificacaoAdapter = ClassificacoesAdapter()
        listViewClassificacao.adapter = classificacaoAdapter
        classificacaoAdapter.notifyDataSetChanged()

        /* TabView */
        tabLayoutClassificacao.setTabGravity(TabLayout.GRAVITY_FILL);

        tabLayoutClassificacao.addTab(tabLayoutClassificacao.newTab().setText(Categoria.LMP2.toString()))
        tabLayoutClassificacao.addTab(tabLayoutClassificacao.newTab().setText(Categoria.LMP3.toString()))
        tabLayoutClassificacao.addTab(tabLayoutClassificacao.newTab().setText(Categoria.GTELM.toString()))

        // fazer pedido a API
        GlobalScope.async {
            classificacoes = Backend.getClassification(this@ClassificacaoFragment.requireContext())

            if (classificacoes.size == 0) {
                this@ClassificacaoFragment.activity?.runOnUiThread {
                    Toast.makeText(
                            this@ClassificacaoFragment.requireContext(),
                            "Não há classificação!",
                            Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                this@ClassificacaoFragment.activity?.runOnUiThread {
                    listaClassificacoes.clear()
                    listaClassificacoes = classificacoes.filter { classificacao ->  classificacao.categoria == Categoria.LMP2 } as MutableList<Classificacao>
                    listaClassificacoes.sortByDescending{ it.pontos }
                    classificacaoAdapter.notifyDataSetChanged()
                }
            }
        }

        tabLayoutClassificacao.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if (tab.position == 0) {
                    listaClassificacoes.clear()
                    listaClassificacoes = classificacoes.filter { classificacao ->  classificacao.categoria == Categoria.LMP2 } as MutableList<Classificacao>
                    listaClassificacoes.sortByDescending{ it.pontos }
                    classificacaoAdapter.notifyDataSetChanged()
                }
                if (tab.position == 1) {
                    listaClassificacoes.clear()
                    listaClassificacoes = classificacoes.filter { classificacao ->  classificacao.categoria == Categoria.LMP3 } as MutableList<Classificacao>
                    listaClassificacoes.sortByDescending{ it.pontos }
                    classificacaoAdapter.notifyDataSetChanged()
                }
                if (tab.position == 2) {
                    listaClassificacoes.clear()
                    listaClassificacoes = classificacoes.filter { classificacao ->  classificacao.categoria == Categoria.GTELM } as MutableList<Classificacao>
                    listaClassificacoes.sortByDescending{ it.pontos }
                    classificacaoAdapter.notifyDataSetChanged()
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        }) /* TabView */

        return rootView
    }

    inner class ClassificacoesAdapter: BaseAdapter() {
        override fun getCount(): Int {
            return listaClassificacoes.size
        }

        override fun getItem(position: Int): Any {
            return listaClassificacoes[position]
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val rowView = layoutInflater.inflate(R.layout.item_row_classificacoes, parent, false)

            val textViewPosicao = rowView.findViewById<TextView>(R.id.textViewItemRowClassificacoesPosicao)
            val textViewNumeroCarro = rowView.findViewById<TextView>(R.id.textViewItemRowClassificacaoNumeroCarro)
            val textViewNomeEquipa = rowView.findViewById<TextView>(R.id.textViewItemRowClassificacaoNomeEquipa)
            val imageViewCarro = rowView.findViewById<ImageView>(R.id.imageViewItemRowClassificacaoImagemCarro)
            val textViewPontos = rowView.findViewById<TextView>(R.id.textViewItemRowClassificacaoPontos)


            textViewPosicao.text = position.plus(1).toString()
            textViewNumeroCarro.text = listaClassificacoes[position].carro?.numero.toString()
            textViewNomeEquipa.text = listaClassificacoes[position].equipa?.nome.toString()
            Picasso.get().load(listaClassificacoes[position].carro?.imagemCarro).fit().centerInside().error(R.drawable.logo_elms).into(imageViewCarro)
            textViewPontos.text = listaClassificacoes[position].pontos.toString()

            // mudar a cor do numero conforme a categoria
            when(listaClassificacoes[position].categoria) {
                Categoria.GTELM -> {
                    val gtelm = this@ClassificacaoFragment.requireContext().getResources().getColor(R.color.GTELM)
                    textViewNumeroCarro.setBackgroundColor(gtelm)
                }
                Categoria.LMP3 -> {
                    val lmp3 = this@ClassificacaoFragment.requireContext().getResources().getColor(R.color.LMP3)
                    textViewNumeroCarro.setBackgroundColor(lmp3)
                }
                Categoria.LMP2 -> {
                    val lmp2 = this@ClassificacaoFragment.requireContext().getResources().getColor(R.color.LMP2)
                    textViewNumeroCarro.setBackgroundColor(lmp2)
                }
            }

            return rowView
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.initail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_total -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.total.pt/"))
                startActivity(browserIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}