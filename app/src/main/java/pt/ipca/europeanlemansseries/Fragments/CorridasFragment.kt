package pt.ipca.europeanlemansseries.Fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.squareup.picasso.Picasso
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import pt.ipca.europeanlemansseries.Backend
import pt.ipca.europeanlemansseries.Models.Corrida
import pt.ipca.europeanlemansseries.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CorridasFragment : Fragment() {

    // Widgets
    lateinit var rootView: View
    lateinit var listViewCorridas: ListView

    var corridas: MutableList<Corrida> = ArrayList()
    lateinit var adapterCorridas: AdapterCorridas

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_corridas, container, false)
        setHasOptionsMenu(true)

        listViewCorridas = rootView.findViewById(R.id.listViewCorridas)

        adapterCorridas = AdapterCorridas()
        listViewCorridas.adapter = adapterCorridas
        adapterCorridas.notifyDataSetChanged()

        GlobalScope.async {
            corridas = Backend.getAllRaces(this@CorridasFragment.requireContext())

            if (corridas.size == 0) {
                this@CorridasFragment.activity?.runOnUiThread {
                    Toast.makeText(
                        this@CorridasFragment.requireContext(),
                        "Não há corridas!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                this@CorridasFragment.activity?.runOnUiThread {
                    adapterCorridas.notifyDataSetChanged()
                }
            }
        }

        return rootView
    }

    inner class AdapterCorridas: BaseAdapter() {
        override fun getCount(): Int {
            return corridas.size
        }

        override fun getItem(position: Int): Any {
            return corridas[position]
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val rowView = layoutInflater.inflate(R.layout.item_row_corridas, parent, false)
            val textViewNumeroCorrida = rowView.findViewById<TextView>(R.id.textViewItemRowCorridasRaceNumber)
            val imageViewCorridaPais = rowView.findViewById<ImageView>(R.id.imageViewItemRowCorridasBandeiraPais)
            val textViewCorridaNome = rowView.findViewById<TextView>(R.id.textViewItemRowCorridaNome)
            val textViewCorridaData = rowView.findViewById<TextView>(R.id.textViewItemRowCorridaData)

            val format = SimpleDateFormat("dd MMMM yyyy", Locale.getDefault())

            textViewNumeroCorrida.text = "".plus(position+1).plus("/").plus(corridas.size)
            Picasso.get().load(corridas[position].bandeiraPais).fit().centerInside().error(R.drawable.logo_elms).into(imageViewCorridaPais)
            textViewCorridaNome.text = corridas[position].nome
            textViewCorridaData.text = format.format(corridas[position].dataFim)

            rowView.isClickable = true
            rowView.setOnClickListener {
                val corridaFragment = CorridaDetailFragment()
                val args = Bundle()
                corridas[position].id?.let { it1 ->
                    args.putInt(CorridaDetailFragment.CORRIDA_ID,
                        it1
                    )
                }
                args.putString(CorridaDetailFragment.CORRIDA_NOME, corridas[position].nome)
                args.putString(CorridaDetailFragment.CORRIDA_PAIS, corridas[position].pais)
                args.putString(CorridaDetailFragment.CORRIDA_BANDEIRA_PAIS, corridas[position].bandeiraPais)
                corridas[position].distancia?.let { it1 ->
                    args.putDouble(CorridaDetailFragment.CORRIDA_DISTANCIA,
                        it1
                    )
                }
                corridas[position].curvas?.let { it1 ->
                    args.putInt(CorridaDetailFragment.CORRIDA_CURVAS,
                        it1
                    )
                }
                args.putString(CorridaDetailFragment.CORRIDA_MORADA, corridas[position].morada)
                args.putString(CorridaDetailFragment.CORRIDA_TELEFONE, corridas[position].telefone)
                args.putString(CorridaDetailFragment.CORRIDA_WEBSITE, corridas[position].website)
                args.putString(CorridaDetailFragment.CORRIDA_FOTO, corridas[position].foto)
                args.putString(CorridaDetailFragment.CORRIDA_TRACK_LAYOUT, corridas[position].trackLayout)
                args.putString(CorridaDetailFragment.CORRIDA_REPLAY, corridas[position].replay)
                corridas[position].dataInicio?.time?.let { it1 ->
                    args.putLong(CorridaDetailFragment.CORRIDA_DATA_INICIO,
                        it1
                    )
                }
                corridas[position].dataFim?.time?.let { it1 ->
                    args.putLong(CorridaDetailFragment.CORRIDA_DATA_FIM,
                        it1
                    )
                }
                corridaFragment.arguments = args

                val transaction = this@CorridasFragment.parentFragmentManager
                transaction.beginTransaction().replace(R.id.nav_host_fragment, corridaFragment).setTransition(
                    FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()
            }

            return rowView
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.initail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_total -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.total.pt/"))
                startActivity(browserIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
