package pt.ipca.europeanlemansseries.Fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import pt.ipca.europeanlemansseries.Adapters.NoticiasAdapter
import pt.ipca.europeanlemansseries.Adapters.PilotosAdapter
import pt.ipca.europeanlemansseries.Backend
import pt.ipca.europeanlemansseries.Models.Carro
import pt.ipca.europeanlemansseries.Models.Enums.Categoria
import pt.ipca.europeanlemansseries.Models.Equipa
import pt.ipca.europeanlemansseries.Models.Noticia
import pt.ipca.europeanlemansseries.Models.Piloto
import pt.ipca.europeanlemansseries.R
import java.util.*
import kotlin.collections.ArrayList

class CarroDetailFragment : Fragment() {

    companion object {
        const val CARRO_ID = "carro_id"
        const val CARRO_ID_EQUIPA = "carro_id_equipa"
        const val CARRO_NOME = "carro_nome"
        const val CARRO_NUMERO = "carro_numero"
        const val CARRO_MODELO = "carro_modelo"
        const val CARRO_FABRICANTE = "carro_fabricante"
        const val CARRO_MARCA_PNEUS = "carro_marca_pneus"
        const val CARRO_IMAGEM_CARRO = "carro_imagem_carro"
    }

    // Widgets
    lateinit var rootView: View
    lateinit var imageViewCarro: ImageView
    lateinit var textViewNomeCarro: TextView
    lateinit var imageViewMarcaPneus: ImageView
    lateinit var textViewCarro: TextView
    lateinit var textViewModeloCarro: TextView
    lateinit var textViewFabricante: TextView
    lateinit var imageViewCategoria: ImageView
    lateinit var imageViewLogoEquipa: ImageView
    lateinit var recyclerViewPilotos: RecyclerView
    lateinit var recyclerViewNoticias: RecyclerView

    // Data model
    lateinit var carro: Carro

    // listas carros
    lateinit var pilotosAdapter: PilotosAdapter
    var pilotos: MutableList<Piloto> = ArrayList()

    // listas noticias
    var noticias: MutableList<Noticia> = ArrayList()
    lateinit var noticiasAdapter: NoticiasAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_carro_detail, container, false)
        setHasOptionsMenu(true)

        // findviewById
        imageViewCarro = rootView.findViewById(R.id.imageViewDetalheCarro)
        textViewNomeCarro = rootView.findViewById(R.id.textViewDetalheCarroNome)
        imageViewMarcaPneus = rootView.findViewById(R.id.imageViewDetalheCarroMarcaPneus)
        textViewCarro = rootView.findViewById(R.id.textViewDetalheCarro)
        textViewModeloCarro = rootView.findViewById(R.id.textViewDetalheCarroModelo)
        textViewFabricante = rootView.findViewById(R.id.textViewDetalheCarroFabricante)
        imageViewCategoria = rootView.findViewById(R.id.imageViewDetalheCarroCategoria)
        imageViewLogoEquipa = rootView.findViewById(R.id.imageViewDetalheCarroLogo)
        recyclerViewPilotos = rootView.findViewById(R.id.recyclerViewCarroDetailPilotos)
        recyclerViewNoticias = rootView.findViewById(R.id.recyclerViewCarroDetailNoticas)

        // criar o objeto
        val idCarro = arguments?.getInt(CARRO_ID)
        val idEquipa = arguments?.getInt(CARRO_ID_EQUIPA)
        val carroNome = arguments?.getString(CARRO_NOME)
        val carroNumero = arguments?.getInt(CARRO_NUMERO)
        val carroModelo = arguments?.getString(CARRO_MODELO)
        val carroFabricante = arguments?.getString(CARRO_FABRICANTE)
        val carroMarcaPneus = arguments?.getString(CARRO_MARCA_PNEUS)
        val carroImagemCarro = arguments?.getString(CARRO_IMAGEM_CARRO)
        carro = Carro(idCarro, carroNome, carroNumero, carroModelo, carroFabricante, carroMarcaPneus, carroImagemCarro)

        // fazer o pedido a API para obter a equipa
        GlobalScope.async {
            carro.equipa =
                idEquipa?.let { Backend.getTeam(this@CarroDetailFragment.requireContext(), it) }

            if (carro.equipa == null) {
                this@CarroDetailFragment.activity?.runOnUiThread {
                    // atualizar a categoria do carro
                    Picasso.get().load("https://live.europeanlemansseries.com/bundles/front/images/logo_elms.png").fit().centerInside().error(R.drawable.logo_elms).into(imageViewLogoEquipa)
                }
            } else {
                this@CarroDetailFragment.activity?.runOnUiThread {
                    // atualizar a categoria do carro
                    if (carro.equipa?.categoria?.equals(Categoria.LMP2) == true)
                        imageViewCategoria.setImageResource(R.drawable.icon_lmp2)
                    else if (carro.equipa?.categoria?.equals(Categoria.LMP3) == true)
                        imageViewCategoria.setImageResource(R.drawable.icon_lmp3)
                    else if (carro.equipa?.categoria?.equals(Categoria.GTELM) == true)
                        imageViewCategoria.setImageResource(R.drawable.icon_lmgte)

                    Picasso.get().load(carro.equipa?.logo).fit().centerInside()
                        .error(R.drawable.logo_elms).into(imageViewLogoEquipa)
                }
            }
        }

        // atualizar o resto da informação
        Picasso.get().load(carro.imagemCarro).fit().centerInside().error(R.drawable.logo_elms).into(imageViewCarro)
        textViewNomeCarro.text = "#".plus(carro.numero).plus(" ").plus(carro.modelo)
        Picasso.get().load(carro.marcaPneus).fit().centerInside().error(R.drawable.logo_elms).into(imageViewMarcaPneus)
        textViewCarro.text = "#".plus(carro.numero).plus(" ").plus(carro.modelo)
        textViewModeloCarro.text = carro.modelo
        textViewFabricante.text = carro.fabricante


        // grelha de pilotos
        pilotosAdapter = PilotosAdapter(pilotos, requireActivity().applicationContext, this.parentFragmentManager)
        pilotosAdapter.notifyDataSetChanged()


        // fazer pedido a API para obter os pilotos
        GlobalScope.async {
            pilotos = Backend.getTeamDrivers(this@CarroDetailFragment.requireContext(), carro.id!!)


            if (pilotos.size == 0) {
                this@CarroDetailFragment.activity?.runOnUiThread {
                    Toast.makeText(
                        this@CarroDetailFragment.requireContext(),
                        "Não há pilotos!",
                        Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                this@CarroDetailFragment.activity?.runOnUiThread {
                    pilotosAdapter = PilotosAdapter(pilotos, requireActivity().applicationContext, this@CarroDetailFragment.parentFragmentManager)
                    val layoutManager = GridLayoutManager(
                        this@CarroDetailFragment.requireContext(),
                        pilotos.size,
                        GridLayoutManager.VERTICAL,
                        false
                    )
                    recyclerViewPilotos.layoutManager = layoutManager
                    pilotosAdapter.notifyDataSetChanged()
                    recyclerViewPilotos.adapter = pilotosAdapter
                }
            }
        }

        noticiasAdapter = NoticiasAdapter(noticias, requireActivity().applicationContext, this.parentFragmentManager)
        noticiasAdapter.notifyDataSetChanged()

        // fazer pedido a API
        GlobalScope.async {
            noticias = Backend.getLastTreeNotices(this@CarroDetailFragment.requireContext())

            if (noticias.size == 0) {
                this@CarroDetailFragment.activity?.runOnUiThread {
                    Toast.makeText(
                        this@CarroDetailFragment.requireContext(),
                        "Não Há notícias!",
                        Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                this@CarroDetailFragment.activity?.runOnUiThread {
                    noticiasAdapter = NoticiasAdapter(noticias, requireActivity().applicationContext, this@CarroDetailFragment.parentFragmentManager)

                    val layoutManager = LinearLayoutManager(
                        this@CarroDetailFragment.requireContext(),
                        LinearLayoutManager.VERTICAL,
                        false
                    )
                    recyclerViewNoticias.layoutManager = layoutManager
                    noticiasAdapter.notifyDataSetChanged()
                    recyclerViewNoticias.adapter = noticiasAdapter

                    noticiasAdapter.notifyDataSetChanged()
                }
            }
        }

        // abrir equipa
        imageViewLogoEquipa.setOnClickListener {
            var equipaDetailFragment = EquipaDetailFragment()
            val args = Bundle()
            carro.equipa?.id?.let { it1 ->
                args.putInt(
                    EquipaDetailFragment.EQUIPA_ID,
                    it1
                )
            }
            args.putString(EquipaDetailFragment.EQUIPA_NOME, carro.equipa?.nome)
            carro.equipa?.categoria?.let { it1 -> Equipa.setCategoria(it1) }?.let { it2 ->
                args.putInt(
                    EquipaDetailFragment.EQUIPA_CATEGORIA,
                    it2
                )
            }
            args.putString(
                EquipaDetailFragment.EQUIPA_BANDEIRA_PAIS,
                carro.equipa?.bandeiraPais
            )
            args.putString(EquipaDetailFragment.EQUIPA_LOGO, carro.equipa?.logo)
            args.putString(EquipaDetailFragment.EQUIPA_FACEBOOK, carro.equipa?.facebook)
            args.putString(EquipaDetailFragment.EQUIPA_TWITTER, carro.equipa?.twitter)
            args.putString(EquipaDetailFragment.EQUIPA_WEBSITE, carro.equipa?.website)
            equipaDetailFragment.arguments = args

            val transaction = this@CarroDetailFragment.parentFragmentManager
            transaction.beginTransaction().replace(R.id.nav_host_fragment, equipaDetailFragment)
                .setTransition(
                    FragmentTransaction.TRANSIT_FRAGMENT_FADE
                ).commit()

        }

        return rootView
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.initail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_total -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.total.pt/"))
                startActivity(browserIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}