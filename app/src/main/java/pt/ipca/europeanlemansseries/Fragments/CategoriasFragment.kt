package pt.ipca.europeanlemansseries.Fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.LinearLayout
import androidx.fragment.app.FragmentTransaction
import pt.ipca.europeanlemansseries.R

class CategoriasFragment : Fragment() {

    // Widgets
    lateinit var rowView: View
    lateinit var linearLayoutLMP2: LinearLayout
    lateinit var linearLayoutLMP3: LinearLayout
    lateinit var linearLayoutLMGTE: LinearLayout

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?): View? {

        rowView = inflater.inflate(R.layout.fragment_categorias, container, false)
        setHasOptionsMenu(true)

        linearLayoutLMP2 = rowView.findViewById(R.id.linearLayoutCategoriaLMP2)
        linearLayoutLMP3 = rowView.findViewById(R.id.linearLayoutCategoriaLMP3)
        linearLayoutLMGTE = rowView.findViewById(R.id.linearLayoutCategoriaLMGTE)

        linearLayoutLMP2.setOnClickListener {
            val lmP2Fragment = LMP2Fragment()

            val transaction = this.parentFragmentManager
            transaction.beginTransaction().replace(R.id.nav_host_fragment, lmP2Fragment).setTransition(
                    FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()
        }

        linearLayoutLMP3.setOnClickListener {
            val lmP3Fragment = LMP3Fragment()

            val transaction = this.parentFragmentManager
            transaction.beginTransaction().replace(R.id.nav_host_fragment, lmP3Fragment).setTransition(
                    FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()
        }

        linearLayoutLMGTE.setOnClickListener {
            val lmgteFragment = LMGTEFragment()

            val transaction = this.parentFragmentManager
            transaction.beginTransaction().replace(R.id.nav_host_fragment, lmgteFragment).setTransition(
                    FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()
        }

        return rowView
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.initail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_total -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.total.pt/"))
                startActivity(browserIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}