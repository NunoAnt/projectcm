package pt.ipca.europeanlemansseries.Fragments

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.view.*
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.squareup.picasso.Picasso
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import pt.ipca.europeanlemansseries.Adapters.NoticiasAdapter
import pt.ipca.europeanlemansseries.Adapters.SliderCarsAdapter
import pt.ipca.europeanlemansseries.Backend
import pt.ipca.europeanlemansseries.Models.Carro
import pt.ipca.europeanlemansseries.Models.Enums.Categoria
import pt.ipca.europeanlemansseries.Models.Equipa
import pt.ipca.europeanlemansseries.Models.Noticia
import pt.ipca.europeanlemansseries.R


class EquipaDetailFragment : Fragment() {

    companion object {
        const val EQUIPA_ID   = "equipa_id"
        const val EQUIPA_NOME = "equipa_nome"
        const val EQUIPA_CATEGORIA = "equipa_categoria"
        const val EQUIPA_BANDEIRA_PAIS = "equipa_bandeira_pais"
        const val EQUIPA_LOGO = "equipa_logo"
        const val EQUIPA_FACEBOOK = "equipa_facebook"
        const val EQUIPA_TWITTER = "equipa_twitter"
        const val EQUIPA_WEBSITE = "equipa_website"
    }

    // Widgets
    lateinit var rootView: View
    lateinit var imageViewLogo: ImageView
    lateinit var textViewNomeEquipa: TextView
    lateinit var imageViewBandeiraPais: ImageView
    lateinit var imageViewCategoria: ImageView
    lateinit var viewPagerCarros: ViewPager2
    lateinit var indicatorsContainer: LinearLayout
    lateinit var recyclerViewNoticias: RecyclerView
    lateinit var imageViewFacebook: ImageView
    lateinit var imageViewTwitter: ImageView
    lateinit var imageViewWebsite: ImageView

    // Data Model
    lateinit var equipa: Equipa

    // lista noticias
    var noticias: MutableList<Noticia> = ArrayList()
    lateinit var noticiasAdapter: NoticiasAdapter


    // slider corridas
    lateinit var sliderCarsAdapter: SliderCarsAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_equipa_detail, container, false)
        setHasOptionsMenu(true)

        // inicializar os widgets
        imageViewLogo = rootView.findViewById(R.id.imageViewDetalheCarroLogo)
        textViewNomeEquipa = rootView.findViewById(R.id.textViewDetalheCarroNome)
        imageViewBandeiraPais = rootView.findViewById(R.id.imageViewDetalheCarroBandeiraPais)
        imageViewCategoria = rootView.findViewById(R.id.imageViewDetalheCarroCategoria)
        viewPagerCarros = rootView.findViewById(R.id.viewPagerEquipaDetailCarros)
        indicatorsContainer = rootView.findViewById(R.id.linearLayoutEquipaDetailIndicators)
        recyclerViewNoticias = rootView.findViewById(R.id.recyclerViewEquipaDetailNoticas)
        imageViewFacebook = rootView.findViewById(R.id.imageViewDetalheCarroFacebook)
        imageViewTwitter = rootView.findViewById(R.id.imageViewDetalheCarroTwitter)
        imageViewWebsite = rootView.findViewById(R.id.imageViewDetalheCarroWebsite)

        // criar o objeto equipa
        val equipaId = arguments?.getInt(EQUIPA_ID)
        val equipaNome = arguments?.getString(EQUIPA_NOME)
        val equipaCategoria = Equipa.checkCategoria(arguments?.getInt(EQUIPA_CATEGORIA)!!)
        val equipaBandeiraPais = arguments?.getString(EQUIPA_BANDEIRA_PAIS)
        val equipaLogo = arguments?.getString(EQUIPA_LOGO)
        val equipaFacebook = arguments?.getString(EQUIPA_FACEBOOK)
        val equipaTwitter = arguments?.getString(EQUIPA_TWITTER)
        val equipaWebsite = arguments?.getString(EQUIPA_WEBSITE)
        equipa = Equipa(equipaId, equipaNome, equipaCategoria, equipaBandeiraPais, equipaLogo, equipaFacebook, equipaTwitter, equipaWebsite)

        // atualizar widgets
        Picasso.get().load(equipaLogo).fit().centerInside().error(R.drawable.logo_elms).into(imageViewLogo)
        textViewNomeEquipa.text = equipaNome
        Picasso.get().load(equipaBandeiraPais).fit().centerInside().error(R.drawable.logo_elms).into(imageViewBandeiraPais)
        if (equipaCategoria.equals(Categoria.LMP2))
            imageViewCategoria.setImageResource(R.drawable.icon_lmp2)
        else if (equipaCategoria.equals(Categoria.LMP3))
            imageViewCategoria.setImageResource(R.drawable.icon_lmp3)
        else if (equipaCategoria.equals(Categoria.GTELM))
            imageViewCategoria.setImageResource(R.drawable.icon_lmgte)

        // abrir link do facebook
        imageViewFacebook.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(equipa.facebook))
            startActivity(browserIntent)
        }

        // abrir link do twitter
        imageViewTwitter.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(equipa.twitter))
            startActivity(browserIntent)
        }

        // abrir link do website
        imageViewWebsite.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(equipa.website))
            startActivity(browserIntent)
        }


        sliderCarsAdapter = SliderCarsAdapter(equipa.carros, this.parentFragmentManager)
        viewPagerCarros.adapter = sliderCarsAdapter

        // lista carros
        // fazer pedido a API
        GlobalScope.async {
            equipa.obterCarros(this@EquipaDetailFragment.requireContext())

            if (equipa.carros.size == 0) {
                this@EquipaDetailFragment.activity?.runOnUiThread {
                    Toast.makeText(
                            this@EquipaDetailFragment.requireContext(),
                            "Não há carros!",
                            Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                this@EquipaDetailFragment.activity?.runOnUiThread {
                    sliderCarsAdapter = SliderCarsAdapter(equipa.carros, this@EquipaDetailFragment.parentFragmentManager)
                    viewPagerCarros.adapter = sliderCarsAdapter
                    sliderCarsAdapter.notifyDataSetChanged()
                }
            }
        }

        // atualizar os indicadores do slider
        setUpIndicators()
        viewPagerCarros.registerOnPageChangeCallback(object :
                ViewPager2.OnPageChangeCallback() {
            override fun onPageScrollStateChanged(position: Int) {
                super.onPageScrollStateChanged(position)
                setCurrentIndicator(position)
            }
        })

        noticiasAdapter = NoticiasAdapter(noticias, requireActivity().applicationContext, this.parentFragmentManager)

        val layoutManager = LinearLayoutManager(
                this@EquipaDetailFragment.requireContext(),
                LinearLayoutManager.VERTICAL,
                false
        )
        recyclerViewNoticias.layoutManager = layoutManager
        noticiasAdapter.notifyDataSetChanged()
        recyclerViewNoticias.adapter = noticiasAdapter

        // fazer pedido a API
        GlobalScope.async {
            noticias = Backend.getLastTreeNotices(this@EquipaDetailFragment.requireContext())

            if (noticias.size == 0) {
                this@EquipaDetailFragment.activity?.runOnUiThread {
                    Toast.makeText(
                            this@EquipaDetailFragment.requireContext(),
                            "Não há notícias!",
                            Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                this@EquipaDetailFragment.activity?.runOnUiThread {
                    noticiasAdapter = NoticiasAdapter(noticias, requireActivity().applicationContext, this@EquipaDetailFragment.parentFragmentManager)

                    val layoutManager = LinearLayoutManager(
                            this@EquipaDetailFragment.requireContext(),
                            LinearLayoutManager.VERTICAL,
                            false
                    )
                    recyclerViewNoticias.layoutManager = layoutManager
                    noticiasAdapter.notifyDataSetChanged()
                    recyclerViewNoticias.adapter = noticiasAdapter

                    noticiasAdapter.notifyDataSetChanged()
                }
            }
        }

        return rootView
    }

    private fun setUpIndicators() {
        val indicators = kotlin.arrayOfNulls<ImageView>(sliderCarsAdapter.itemCount)
        val layoutParams: LinearLayout.LayoutParams =
                LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
        layoutParams.setMargins(8, 0, 8, 0)
        for (i in indicators.indices) {
            indicators[i] = ImageView(activity?.applicationContext)
            indicators[i].apply {
                this?.setImageDrawable(
                        activity?.applicationContext?.let {
                            ContextCompat.getDrawable(
                                    it,
                                    R.drawable.indicator_inactive
                            )
                        }
                )
                this?.layoutParams = layoutParams
            }
            indicatorsContainer.addView(indicators[i])
        }
    }

    private fun setCurrentIndicator(index: Int) {
        val childCount = indicatorsContainer.childCount
        for (i in 0 until childCount) {
            val imageView = indicatorsContainer[i] as ImageView
            if (i == index) {
                imageView.setImageDrawable(
                        activity?.applicationContext?.let {
                            ContextCompat.getDrawable(
                                    it,
                                    R.drawable.indicator_active
                            )
                        }
                )
            } else {
                imageView.setImageDrawable(
                        activity?.applicationContext?.let {
                            ContextCompat.getDrawable(
                                    it,
                                    R.drawable.indicator_inactive
                            )
                        }
                )
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.initail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_total -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.total.pt/"))
                startActivity(browserIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}