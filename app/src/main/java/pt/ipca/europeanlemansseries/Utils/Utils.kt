package pt.ipca.europeanlemansseries.Utils

import com.toxicbakery.bcrypt.Bcrypt
import java.text.DecimalFormat
import java.util.regex.Pattern

class Utils {

    companion object {
        fun isEmailValid(email : String) : Boolean {
            val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
            val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(email)
            return matcher.matches()
        }

        fun kmToMiles(km: Double): Double {
            val miles = km * 0.62137

            return miles
        }
    }

}