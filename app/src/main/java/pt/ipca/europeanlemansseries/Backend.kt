package pt.ipca.europeanlemansseries

import android.content.Context
import android.util.Log
import android.widget.Toast
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import pt.ipca.europeanlemansseries.Models.*
import pt.ipca.europeanlemansseries.Models.Enums.Categoria

import pt.ipca.europeanlemansseries.VolleyHelper.VolleyHelper
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class Backend {

    companion object {

        // realizar pedido a API para inserir utilizador
        suspend fun registarUtilizador(utilizador :  Utilizador, context : Context) : String = suspendCoroutine {
            var result = ""

            VolleyHelper.getInstance()
                .add(context, "/user/insert", utilizador.toJson(), object : VolleyHelper.OnAddListener {
                    override fun onSucess(data: String) {
                        try {
                            val jsonObject = JSONObject(data)
                            result = jsonObject.getString("status")
                            it.resume(result)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                            Log.e("erro login", e.toString())
                        }
                    }

                    override fun onError(error: String) {
                        result = "error"
                        Log.e("erro login", error.toString())
                        Log.e("result", result)
                        it.resume(result)
                    }
                })
        }

        suspend fun loginUtilizador(utilizador: Utilizador, context: Context) : String = suspendCoroutine {
            var result = ""

            VolleyHelper.getInstance()
                .login(context, "/user/login", utilizador.toJson(), object : VolleyHelper.OnAddListener {
                    override fun onSucess(data: String) {
                        try {
                            val jsonObject = JSONObject(data)
                            result = jsonObject.getString("status")
                            it.resume(result)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(error: String) {
                        result = "error"
                        it.resume(result)
                    }
                }
            )
        }

        suspend fun getAllRaces(context: Context) : MutableList<Corrida> = suspendCoroutine {
            var corridas:  MutableList<Corrida> = ArrayList()

            VolleyHelper.getInstance().getAllRaces(context, "/races", object : VolleyHelper.OnGetDataListener {
                override fun onSucess(data: String) {
                    try {
                        corridas.clear()
                        val jsonArray = JSONArray(data)
                        for (i in 0 until jsonArray.length()) {
                            val c = Corrida.fromJson(jsonArray.getJSONObject(i))
                            corridas.add(c)
                        }
                        it.resume(corridas)
                    } catch (e: JSONException) {
                        Toast.makeText(context, "Erro ao obter as corridas!",Toast.LENGTH_LONG).show()
                        e.printStackTrace()
                    }

                }

                override fun onError(error: String) {
                    Toast.makeText(context, "Erro ao obter as corridas!",Toast.LENGTH_LONG).show()
                    it.resume(corridas)
                }
            })
        }

        suspend fun getLastTreeNotices(context: Context) : MutableList<Noticia> = suspendCoroutine {
            var noticias:  MutableList<Noticia> = ArrayList()

            VolleyHelper.getInstance().getLastTreeNotices(context, "/notices/last", object : VolleyHelper.OnGetDataListener {
                override fun onSucess(data: String) {
                    try {
                        noticias.clear()
                        val jsonArray = JSONArray(data)
                        for (i in 0 until jsonArray.length()) {
                            val n = Noticia.fromJson(jsonArray.getJSONObject(i))
                            noticias.add(n)
                        }
                        it.resume(noticias)
                    } catch (e: JSONException) {
                        Toast.makeText(context, "Erro ao obter as notícias!",Toast.LENGTH_LONG).show()
                        e.printStackTrace()
                    }

                }

                override fun onError(error: String) {
                    Toast.makeText(context, "Erro ao obter as notícias!",Toast.LENGTH_LONG).show()
                    it.resume(noticias)
                }
            })
        }

        suspend fun getAllNotices(context: Context) : MutableList<Noticia> = suspendCoroutine {
            var noticias:  MutableList<Noticia> = ArrayList()

            VolleyHelper.getInstance().getAllNotices(context, "/notices", object : VolleyHelper.OnGetDataListener {
                override fun onSucess(data: String) {
                    try {
                        noticias.clear()
                        val jsonArray = JSONArray(data)
                        for (i in 0 until jsonArray.length()) {
                            val n = Noticia.fromJson(jsonArray.getJSONObject(i))
                            noticias.add(n)
                        }
                        it.resume(noticias)
                    } catch (e: JSONException) {
                        Toast.makeText(context, "Erro ao obter as notícias!",Toast.LENGTH_LONG).show()
                        e.printStackTrace()
                    }

                }

                override fun onError(error: String) {
                    Toast.makeText(context, "Erro ao obter as notícias!",Toast.LENGTH_LONG).show()
                    it.resume(noticias)
                }
            })
        }


        suspend fun getAllTeams(context: Context) : MutableList<Equipa> = suspendCoroutine {
            var equipas:  MutableList<Equipa> = ArrayList()

            VolleyHelper.getInstance().getAllTeams(context, "/teams", object : VolleyHelper.OnGetDataListener {
                override fun onSucess(data: String) {
                    try {
                        equipas.clear()
                        val jsonArray = JSONArray(data)
                        for (i in 0 until jsonArray.length()) {
                            val e = Equipa.fromJson(jsonArray.getJSONObject(i))
                            equipas.add(e)
                        }
                        it.resume(equipas)
                    } catch (e: JSONException) {
                        Toast.makeText(context, "Erro ao obter as equipas!",Toast.LENGTH_LONG).show()
                        e.printStackTrace()
                    }

                }

                override fun onError(error: String) {
                    Toast.makeText(context, "Erro ao obter as equipas!",Toast.LENGTH_LONG).show()
                    it.resume(equipas)
                }
            })
        }

        suspend fun getTeamsCars(context: Context, id: Int) : MutableList<Carro> = suspendCoroutine {
            var carros:  MutableList<Carro> = ArrayList()

            VolleyHelper.getInstance().getTeamCars(context, "/cars/", id, object : VolleyHelper.OnGetDataListener {
                override fun onSucess(data: String) {
                    try {
                        carros.clear()
                        val jsonArray = JSONArray(data)
                        for (i in 0 until jsonArray.length()) {
                            val c = Carro.fromJson(jsonArray.getJSONObject(i))
                            carros.add(c)
                        }
                        it.resume(carros)
                    } catch (e: JSONException) {
                        Toast.makeText(context, "Erro ao obter os carros das equipas!",Toast.LENGTH_LONG).show()
                        e.printStackTrace()
                    }

                }

                override fun onError(error: String) {
                    Toast.makeText(context, "Erro ao obter os carros das equipas!",Toast.LENGTH_LONG).show()
                    it.resume(carros)
                }
            })
        }

        suspend fun getTeam(context: Context, id: Int) : Equipa = suspendCoroutine {
            var equipa: Equipa

            VolleyHelper.getInstance().getTeam(context, "/team/", id, object : VolleyHelper.OnGetDataListener {
                override fun onSucess(data: String) {
                    try {
                        val jsonObject = JSONObject(data)
                        equipa = Equipa.fromJson(jsonObject)
                        it.resume(equipa)
                    } catch (e: JSONException) {
                        Toast.makeText(context, "Erro ao obter a equipa!", Toast.LENGTH_LONG).show()
                        e.printStackTrace()
                    }
                }

                override fun onError(error: String) {
                    Toast.makeText(context, "Erro ao obter a equipa!", Toast.LENGTH_LONG).show()
                }
            })
        }

        suspend fun getCars(context: Context) : MutableList<Carro> = suspendCoroutine {
            var carros:  MutableList<Carro> = ArrayList()

            VolleyHelper.getInstance().getCars(context, "/cars", object : VolleyHelper.OnGetDataListener {
                override fun onSucess(data: String) {
                    try {
                        carros.clear()
                        val jsonArray = JSONArray(data)
                        for (i in 0 until jsonArray.length()) {
                            val c = Carro.fromJson(jsonArray.getJSONObject(i))
                            carros.add(c)
                        }
                        it.resume(carros)
                    } catch (e: JSONException) {
                        Toast.makeText(context, "Erro ao obter os carros das equipas!",Toast.LENGTH_LONG).show()
                        e.printStackTrace()
                    }

                }

                override fun onError(error: String) {
                    Toast.makeText(context, "Erro ao obter os carros das equipas!",Toast.LENGTH_LONG).show()
                    it.resume(carros)
                }
            })
        }

        suspend fun getTeamDrivers(context: Context, id: Int) : MutableList<Piloto> = suspendCoroutine {
            var pilotos:  MutableList<Piloto> = ArrayList()

            VolleyHelper.getInstance().getCarDrivers(context, "/drivers/", id, object : VolleyHelper.OnGetDataListener {
                override fun onSucess(data: String) {
                    try {
                        pilotos.clear()
                        val jsonArray = JSONArray(data)
                        for (i in 0 until jsonArray.length()) {
                            val p = Piloto.fromJson(jsonArray.getJSONObject(i))
                            pilotos.add(p)
                        }
                        it.resume(pilotos)
                    } catch (e: JSONException) {
                        Toast.makeText(context, "Erro ao obter os pilotos da equipa!", Toast.LENGTH_LONG).show()
                        e.printStackTrace()
                    }
                }

                override fun onError(error: String) {
                    Toast.makeText(context, "Erro ao obter os pilotos da equipa!", Toast.LENGTH_LONG).show()
                }
            })
        }

        suspend fun getDrivers(context: Context) : MutableList<Piloto> = suspendCoroutine {
            var pilotos:  MutableList<Piloto> = ArrayList()

            VolleyHelper.getInstance().getDrivers(context, "/drivers", object : VolleyHelper.OnGetDataListener {
                override fun onSucess(data: String) {
                    try {
                        pilotos.clear()
                        val jsonArray = JSONArray(data)
                        for (i in 0 until jsonArray.length()) {
                            val p = Piloto.fromJson(jsonArray.getJSONObject(i))
                            pilotos.add(p)
                        }
                        it.resume(pilotos)
                    } catch (e: JSONException) {
                        Toast.makeText(context, "Erro ao obter os pilotos!",Toast.LENGTH_LONG).show()
                        e.printStackTrace()
                    }

                }

                override fun onError(error: String) {
                    Toast.makeText(context, "Erro ao obter os pilotos!",Toast.LENGTH_LONG).show()
                    it.resume(pilotos)
                }
            })
        }

        suspend fun getCar(context: Context, id: Int) : Carro = suspendCoroutine {
            var carro: Carro

            VolleyHelper.getInstance().getTeam(context, "/car/", id, object : VolleyHelper.OnGetDataListener {
                override fun onSucess(data: String) {
                    try {
                        val jsonObject = JSONObject(data)
                        carro = Carro.fromJson(jsonObject)
                        it.resume(carro)
                    } catch (e: JSONException) {
                        Toast.makeText(context, "Erro ao obter o carro!", Toast.LENGTH_LONG).show()
                        e.printStackTrace()
                    }
                }

                override fun onError(error: String) {
                    Toast.makeText(context, "Erro ao obter o carro!", Toast.LENGTH_LONG).show()
                }
            })
        }

        suspend fun getSessonsRaces(context: Context, id: Int) : MutableList<Sessao> = suspendCoroutine {
            var sessoes:  MutableList<Sessao> = ArrayList()

            VolleyHelper.getInstance().getRaceSessons(context, "/sessons/", id, object : VolleyHelper.OnGetDataListener {
                override fun onSucess(data: String) {
                    try {
                        sessoes.clear()
                        val jsonArray = JSONArray(data)
                        for (i in 0 until jsonArray.length()) {
                            val s = Sessao.fromJson(jsonArray.getJSONObject(i))
                            sessoes.add(s)
                        }
                        it.resume(sessoes)
                    } catch (e: JSONException) {
                        Toast.makeText(context, "Erro ao obter as sessoes!", Toast.LENGTH_LONG).show()
                        e.printStackTrace()
                    }
                }

                override fun onError(error: String) {
                    Toast.makeText(context, "Erro ao obter as sessoes!", Toast.LENGTH_LONG).show()
                    it.resume(sessoes)
                }
            })
        }

        suspend fun getRaceResult(context: Context, id: Int) : MutableList<Resultado> = suspendCoroutine {
            var resultados:  MutableList<Resultado> = ArrayList()

            VolleyHelper.getInstance().getRaceResult(context, "/result/", id, object : VolleyHelper.OnGetDataListener {
                override fun onSucess(data: String) {
                    try {
                        resultados.clear()
                        val jsonArray = JSONArray(data)
                        for (i in 0 until jsonArray.length()) {
                            val r = Resultado.fromJson(jsonArray.getJSONObject(i))
                            resultados.add(r)
                        }
                        it.resume(resultados)
                    } catch (e: JSONException) {
                        Toast.makeText(context, "Erro ao obter os resultados!", Toast.LENGTH_LONG).show()
                        e.printStackTrace()
                    }
                }

                override fun onError(error: String) {
                    Toast.makeText(context, "Erro ao obter os resultados!", Toast.LENGTH_LONG).show()
                    it.resume(resultados)
                }
            })
        }

        suspend fun getClassification(context: Context) : MutableList<Classificacao> = suspendCoroutine {
            var classificacaoes:  MutableList<Classificacao> = ArrayList()

            VolleyHelper.getInstance().getClassification(context, "/classification", object : VolleyHelper.OnGetDataListener {
                override fun onSucess(data: String) {
                    try {
                        classificacaoes.clear()
                        val jsonArray = JSONArray(data)
                        for (i in 0 until jsonArray.length()) {
                            val c = Classificacao.fromJson(jsonArray.getJSONObject(i))
                            classificacaoes.add(c)
                        }
                        it.resume(classificacaoes)
                    } catch (e: JSONException) {
                        Toast.makeText(context, "Erro ao obter a classificação!",Toast.LENGTH_LONG).show()
                        e.printStackTrace()
                    }

                }

                override fun onError(error: String) {
                    Toast.makeText(context, "Erro ao obter a classificação!",Toast.LENGTH_LONG).show()
                    it.resume(classificacaoes)
                }
            })
        }
    }
}